/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        
        String url = "";
        String message = "";
        
        HttpSession session = request.getSession();
        
        // Get form parameters
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        
        // Check if the auth code has been cleared, if not, user has not confirmed email address
        if (UserDB.getAuthCode(email).equalsIgnoreCase("null")) {
            // Check if user exists in DB
            if (UserDB.userExists(email, password)) {
                url = "/profile.jsp";
                
                // Change session ID
                request.changeSessionId();
                session = request.getSession();

                // Get user information from DB
                User user = new User();
                user = UserDB.getUser(email);  
                
                // Check if user claims to be associated with an employer
                if (user.getEmployer()) { 
                    Employer employer = new Employer();
                    
                    // User claims employer account, check if the user is associated with any employers
                    int eid = EmployerDB.checkUserAssociated(user.getID());
                    
                    if (eid != -1) {
                        // User is associated with an employer, get the employer
                        employer = EmployerDB.getEmployer(eid);
                        
                        if (employer.hasJobs()) {
                            // Get number of jobs employer hsa posted
                            int numberOfJobs = employer.numberOfJobs();
                            
                            Job[] jobs = new Job[numberOfJobs];
                            UserProfile[] userProfiles = new UserProfile[numberOfJobs];
                            User[] users = new User[numberOfJobs];
                            
                            for (int i = 0; i < numberOfJobs; i++) {
                                // Display jobs in reverse order of creation
                                int jid = employer.getJob(employer.numberOfJobs() - i - 1);
                                //System.out.println("Login jobID: " + jid);
                                jobs[i] = JobDB.getJob(jid);
                                
                                // Select a random user from a group of users who best match the position skill requirements based on the tolerance
                                UserProfile up = UserProfileDB.searchUserProfileRandom(3, jobs[i].getSkills(), Employer.TOLERANCE);
                                if (!(up == null)) {
                                    users[i] = UserDB.getUser(up.getUserID());
                                } else {
                                    users[i] = new User();
                                }
                            }
                            
                            session.setAttribute("jobs", jobs);
                            session.setAttribute("users", users);
                        }
                        
                        session.setAttribute("employer", employer);
                    } else {
                        // User is not associated with an employer but is set for an employer account
                        Job[] jobs = null;
                        session.setAttribute("jobs", jobs);
                    }
                    
                    session.setAttribute("employer", employer);
                } else {
                    // User is not an employer account, create user profile
                    UserProfile userProfile = new UserProfile();
                    
                    // Check if user profile exists
                    if (UserProfileDB.checkUserProfileExists(user.getID())) {
                        // Get user profile
                        userProfile = UserProfileDB.getUserProfile(user.getID());
                    
                        Job[] jobs = new Job[UserProfile.JOBS_TO_DISPLAY];
                        
                        // Query Job DB for positions that match desired skills
                        jobs = JobDB.searchJobs(UserProfile.JOBS_TO_DISPLAY, userProfile.getCareerSkills(), userProfile.TOLERANCE);
                        
                        session.setAttribute("jobs", jobs);
                    } else {
                        Job[] jobs = null;
                        session.setAttribute("jobs", jobs);
                    }
                   
                    session.setAttribute("userProfile", userProfile);
                }
                
                session.setAttribute("user", user);
            } else {
                // User does not exist or incorrect authentication 
                message = "Login failed!";
                url = "/login.jsp";
                                
                session.setAttribute("message", message);
            }
        } else {
            // Auth code hasn't been cleared, user hasn't confirmed email
            message = "Please confirm your email address!";
            url = "/login.jsp";
            
            session.setAttribute("message", message);
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        /*HttpSession session = request.getSession();
        String s_course = request.getParameter("course");
        Registrant registrant = (Registrant) session.getAttribute("registrant");
        String url = "";
        
        if (registrant == null) {
            url = "/index.jsp";
        } else {
            url = "/results.jsp";
            
            registrant.removeCourse(s_course);
            
            session.setAttribute("registrant", registrant);
        }
                
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
        */
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Login servlet";
    }// </editor-fold>

}
