/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author astla
 */
public class JobDB {
    
    public static synchronized boolean jobExists(Job job) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        // Get the salt and if user is enabled
        String query = "SELECT * FROM jobs WHERE id = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, job.getID());
            rs = ps.executeQuery();
            if (rs.first()) { 
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println("JobDB.jobExists(int) " + e);
            
            return false;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean jobExists(int jid) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        // Get the salt and if user is enabled
        String query = "SELECT * FROM jobs WHERE id = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, jid);
            rs = ps.executeQuery();
            if (rs.first()) { 
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println("JobDB.jobExists(int) " + e);
            
            return false;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean addJob(Job job) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String insert = "INSERT INTO jobs (USERID, EMPLOYERID, TITLE, SUMMARY, SALARY, " +
                "YEARSEXP, RESP1, RESP2, RESP3, RESP4, RESP5, SKILL1, SKILL2, SKILL3, SKILL4, " +
                "SKILL5, SKILL6, SKILL7, SKILL8, SKILL9,SKILL10, EMPTYPE, CITY, STATE, ZIP, EDUCATION) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                
        String[] responsibilities = new String[Job.RESPONSIBILITIES_SIZE];
        responsibilities = job.getResponsibilities();
        
        int[] skills = new int[Job.SKILLS_SIZE];
        skills = job.getSkills();
                
        try {
            ps = connection.prepareStatement(insert);
            ps.setInt(1, job.getUserID());
            ps.setInt(2, job.getEmployerID());
            ps.setString(3, job.getTitle());
            ps.setString(4, job.getSummary());
            ps.setString(5, job.getSalary());
            ps.setString(6, job.getYearsExperience());
            
            for (int i = 0; i < Job.RESPONSIBILITIES_SIZE; i++) {
                ps.setString(7 + i, responsibilities[i]);
            }
            
            for (int i = 0; i < Job.SKILLS_SIZE; i++) {
                ps.setInt(12 + i, skills[i]);
            }
            
            ps.setInt(18, 0);
            ps.setInt(19, 0);
            ps.setInt(20, 0);
            ps.setInt(21, 0);
            ps.setString(22, job.getEmploymentType());
            ps.setString(23, job.getCity());
            ps.setString(24, job.getState());
            ps.setString(25, job.getZip());
            ps.setString(26, job.getEducation());
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("JobDB.addJob(Job) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean addJob(int uid, int eid, String title, String summary, String salary, String yearsExperience, String resp1, String resp2, String resp3, String resp4, String resp5, int skill1, int skill2, int skill3, int skill4, int skill5, int skill6, int skill7, int skill8, int skill9, int skill10, String employmentType, String city, String state, String zip, String education) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String insert = "INSERT INTO jobs (USERID, EMPLOYERID, TITLE, SUMMARY, SALARY, " +
                "YEARSEXP, RESP1, RESP2, RESP3, RESP4, RESP5, SKILL1, SKILL2, SKILL3, SKILL4, " +
                "SKILL5, SKILL6, SKILL7, SKILL8, SKILL9,SKILL10, EMPTYPE, CITY, STATE, ZIP, EDUCATION) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                
        try {
            ps = connection.prepareStatement(insert);
            ps.setInt(1, uid);
            ps.setInt(2, eid);
            ps.setString(3, title);
            ps.setString(4, summary);
            ps.setString(5, salary);
            ps.setString(6, yearsExperience);
            ps.setString(7, resp1);
            ps.setString(8, resp2);
            ps.setString(9, resp3);
            ps.setString(10, resp4);
            ps.setString(11, resp5);
            ps.setInt(12, skill1);
            ps.setInt(13, skill2);
            ps.setInt(14, skill3);
            ps.setInt(15, skill4);
            ps.setInt(16, skill5);
            ps.setInt(17, skill6);
            ps.setInt(18, skill7);
            ps.setInt(19, skill8);
            ps.setInt(20, skill9);
            ps.setInt(21, skill10);
            ps.setString(22, employmentType);
            ps.setString(23, city);
            ps.setString(24, state);
            ps.setString(25, zip);
            ps.setString(26, education);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("JobDB.addJob(...) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean deleteJob(Job job) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        // Get the salt and if user is enabled
        String delete = "DELETE FROM jobs WHERE id = ?";
        try {
            ps = connection.prepareStatement(delete);
            ps.setInt(1, job.getID());
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("JobDB.deleteJob(Job) " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }    
    }
    
    public static synchronized boolean deleteJob(int jid) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        // Get the salt and if user is enabled
        String delete = "DELETE FROM jobs WHERE id = ?";
        try {
            ps = connection.prepareStatement(delete);
            ps.setInt(1, jid);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("JobDB.deleteJob(int) " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized Job getJob(int jid) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Job job = new Job();
        
        String query = "SELECT * FROM jobs WHERE id = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, jid);
            rs = ps.executeQuery();
            
            if (rs.first()) {
                job.setID(rs.getInt("ID"));
                job.setUserID(rs.getInt("USERID"));
                job.setEmployerID(rs.getInt("EMPLOYERID"));
                job.setTitle(rs.getString("TITLE"));
                job.setSummary(rs.getString("SUMMARY"));
                job.setSalary(rs.getString("SALARY"));
                job.setYearsExperience(rs.getString("YEARSEXP"));
                
                String[] responsibilities = new String[Job.RESPONSIBILITIES_SIZE];
                for (int i = 0; i < Job.RESPONSIBILITIES_SIZE; i++) {
                    String param = "RESP" + Integer.toString(i + 1);
                    responsibilities[i] = rs.getString(param);
                }
                job.setResponsibilities(responsibilities);
                
                int[] skills = new int[Job.SKILLS_SIZE];
                for (int i = 0; i < Job.SKILLS_SIZE; i++) {
                    String param = "SKILL" + Integer.toString(i + 1);
                    skills[i] = rs.getInt(param);
                }
                job.setSkills(skills);
                
                job.setEmploymentType(rs.getString("EMPTYPE"));
                job.setCity(rs.getString("CITY"));
                job.setState(rs.getString("STATE"));
                job.setZip(rs.getString("ZIP"));
                job.setEducation(rs.getString("EDUCATION"));
            }
        } catch (SQLException e) {
            System.err.println("JobDB.getJob(int) - " + e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);            
        }
        
        return job;
    }
    
    public static synchronized Job getJob(Job j) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Job job = new Job();
        
        String query = "SELECT * FROM jobs WHERE id = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, j.getID());
            rs = ps.executeQuery();
            
            if (rs.first()) {
                job.setID(rs.getInt("ID"));
                job.setUserID(rs.getInt("USERID"));
                job.setEmployerID(rs.getInt("EMPLOYERID"));
                job.setTitle(rs.getString("TITLE"));
                job.setSummary(rs.getString("SUMMARY"));
                job.setSalary(rs.getString("SALARY"));
                job.setYearsExperience(rs.getString("YEARSEXP"));
                
                String[] responsibilities = new String[Job.RESPONSIBILITIES_SIZE];
                for (int i = 0; i < Job.RESPONSIBILITIES_SIZE; i++) {
                    String param = "RESP" + Integer.toString(i + 1);
                    responsibilities[i] = rs.getString(param);
                }
                job.setResponsibilities(responsibilities);
                
                int[] skills = new int[Job.SKILLS_SIZE];
                for (int i = 0; i < Job.SKILLS_SIZE; i++) {
                    String param = "SKILL" + Integer.toString(i + 1);
                    skills[i] = rs.getInt(param);
                }
                job.setSkills(skills);
                
                job.setEmploymentType(rs.getString("EMPTYPE"));
                job.setCity(rs.getString("CITY"));
                job.setState(rs.getString("STATE"));
                job.setZip(rs.getString("ZIP"));
                job.setEducation(rs.getString("EDUCATION"));
            }
        } catch (SQLException e) {
            System.err.println("JobDB.getJob(Job) - " + e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);            
        }
        
        return job;
    } 
    
    public static synchronized int getJobID(Job j) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Job job = new Job();
        
        String query = "SELECT id FROM jobs WHERE userid = ? AND employerid = ? AND title = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, j.getUserID());
            ps.setInt(2, j.getEmployerID());
            ps.setString(3, j.getTitle());
            rs = ps.executeQuery();
            
            if (rs.first()) {
                return rs.getInt("ID");
            } else {
                DBUtil.closeResultSet(rs);
                DBUtil.closePreparedStatement(ps);
                pool.freeConnection(connection);  
                
                return -1;
            }
        } catch (SQLException e) {
            System.err.println("JobDB.getJob(Job) - " + e);
            
            return -1;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);            
        }
    }
    
    public static synchronized boolean updateJob(Job job) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String update = "UPDATE jobs SET TITLE=?, SUMMARY=?, SALARY=?, EMPTYPE=?, YEARSEXP=?";
        String[] responsibilities = new String[Job.RESPONSIBILITIES_SIZE];
        responsibilities = job.getResponsibilities();
        for (int i = 0; i < Job.RESPONSIBILITIES_SIZE; i++) {
            update += ", RESP" + Integer.toString(i + 1) + "=?";
        }
                
        int[] skills = new int[Job.SKILLS_SIZE];
        skills = job.getSkills();
        for (int i = 0; i < Job.SKILLS_SIZE; i++) {
            update += ", SKILL" + Integer.toString(i + 1) + "=?";
        }
                
        update += ", CITY=?, STATE=?, ZIP=?, EDUCATION=? WHERE id=?;";
        
        try {
            ps = connection.prepareStatement(update);
            
            ps.setString(1, job.getTitle());
            ps.setString(2, job.getSummary());
            ps.setString(3, job.getSalary());
            ps.setString(4, job.getEmploymentType());
            ps.setString(5, job.getYearsExperience());
            
            for (int i = 0; i < Job.RESPONSIBILITIES_SIZE; i++) {
                ps.setString(6 + i, responsibilities[i]);
            }
            
            for (int i = 0; i < Job.SKILLS_SIZE; i++) {
                ps.setInt(11 + i, skills[i]);
            }
            
            ps.setString(17, job.getCity());
            ps.setString(18, job.getState());
            ps.setString(19, job.getZip());
            ps.setString(20, job.getEducation());
            ps.setInt(21, job.getID());
    
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("JobDB.updateJob(Job) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized Job[] searchJobs(int numberOfJobs, int[] careerSkills, int tolerance) {
        // numberOfJobs to return
        // desired skills user wants to search jobs for
        // tolerance is the positive and negative distance from desired skill value that will match
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        int count = 0;
        int jobsCounter = 0;
        Job[] jobs = new Job[numberOfJobs];
        
        // Prepare the query to find out if any jobs exist in the Job DB that match the user's desired skills
        String queryCount = "SELECT COUNT(*) AS rows FROM jobs WHERE (";
        for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
            queryCount += "SKILL" + Integer.toString(i + 1) + " BETWEEN ? AND ?)";
            if (i < UserProfile.CAREER_SKILLS_SIZE - 1) {
                queryCount += " AND (";
            }
        }
       
        try {
            ps = connection.prepareStatement(queryCount);
            
            for (int i = 1; i < UserProfile.CAREER_SKILLS_SIZE + 1; i++) {
                ps.setInt((2*i) - 1, careerSkills[i - 1] - tolerance);
                ps.setInt((2*i), careerSkills[i - 1] + tolerance);
            }
            
            rs = ps.executeQuery();
            
            if (rs.first()) {
                //System.out.println("rows: " + rs.getInt("rows"));
                
                // At least one job was found matching the user's desired career skills
                count = rs.getInt("rows");            
            } else {
                return null;
            }
        } catch (SQLException e) {
            System.err.println("JobDB.searchJobs(Job[], int, int) - " + e);
            
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
            
            return null;
        }
        
        // Jobs that match the user's career desires exist, get the job details
        if (count > 0) {
            // Prepare the query to get job details for the matching jobs
            String query = "SELECT * FROM jobs WHERE (";
            for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                query += "SKILL" + Integer.toString(i + 1) + " BETWEEN ? AND ?)";
                if (i < UserProfile.CAREER_SKILLS_SIZE - 1) {
                    query += " AND (";
                }
            }
         
            try {
                ps = connection.prepareStatement(query);

                for (int i = 1; i < UserProfile.CAREER_SKILLS_SIZE + 1; i++) {
                    ps.setInt((2*i) - 1, careerSkills[i - 1] - tolerance);
                    ps.setInt((2*i), careerSkills[i - 1] + tolerance);
                }

                rs = ps.executeQuery();

                while ((rs.next()) && (jobsCounter < numberOfJobs)) {
                    Job job = new Job();

                    job.setID(rs.getInt("ID"));
                    job.setUserID(rs.getInt("USERID"));
                    job.setEmployerID(rs.getInt("EMPLOYERID"));
                    job.setTitle(rs.getString("TITLE"));
                    job.setSummary(rs.getString("SUMMARY"));
                    job.setSalary(rs.getString("SALARY"));
                    job.setYearsExperience(rs.getString("YEARSEXP"));

                    String[] responsibilities = new String[Job.RESPONSIBILITIES_SIZE];
                    for (int i = 0; i < Job.RESPONSIBILITIES_SIZE; i++) {
                        String param = "RESP" + Integer.toString(i + 1);
                        responsibilities[i] = rs.getString(param);
                    }
                    job.setResponsibilities(responsibilities);

                    int[] skills = new int[Job.SKILLS_SIZE];
                    for (int i = 0; i < Job.SKILLS_SIZE; i++) {
                        String param = "SKILL" + Integer.toString(i + 1);
                        skills[i] = rs.getInt(param);
                    }
                    job.setSkills(skills);

                    job.setEmploymentType(rs.getString("EMPTYPE"));
                    job.setCity(rs.getString("CITY"));
                    job.setState(rs.getString("STATE"));
                    job.setZip(rs.getString("ZIP"));
                    job.setEducation(rs.getString("EDUCATION"));

                    jobs[jobsCounter] = job;
                    jobsCounter++;
                }
                
                // Fill up the remaining jobs with null jobs
                if (jobsCounter < numberOfJobs) {
                    while (jobsCounter < numberOfJobs) {
                        jobs[jobsCounter] = new Job();
                        jobsCounter++;
                    }
                }
            } catch (SQLException e) {
                System.err.println("JobDB.searchJobs(Job[], int, int) - " + e);

                return null;
            } finally {
                DBUtil.closeResultSet(rs);
                DBUtil.closePreparedStatement(ps);
                pool.freeConnection(connection);            
            }
        } else {
            return null;
        }
        
        return jobs;
    }
}
