/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author astla
 */
public class User {
    private int u_ID;
    private String u_Email;
    private String u_FirstName;
    private String u_LastName;
    private String u_Phone;
    private String u_Address;
    private String u_City;
    private String u_State;
    private String u_Zip;
    private boolean u_Employer;
    private String u_EmployerName;
    private int u_EmployerID;
    private boolean u_Enabled;

    public User() {
        u_ID = -1;
        u_FirstName = "";
        u_LastName = "";
        u_Email = "";
        u_Phone = "";
        u_Address = "";
        u_City = "";
        u_State = "";
        u_Zip = "";
        u_Employer = false;
        u_EmployerName = "";
        u_EmployerID = -1;
        u_Enabled = false;
    }
    
    public User(int id, String firstName, String lastName, String email, boolean enabled) {
        this.u_ID = id;
        this.u_FirstName = firstName;
        this.u_LastName = lastName;
        this.u_Email = email;
        this.u_Phone = "";
        this.u_Address = "";
        this.u_City = "";
        this.u_State = "";
        this.u_Zip = "";
        this.u_Employer = false;
        this.u_EmployerName = "";
        this.u_EmployerID = -1;
        this.u_Enabled = enabled;
    }
    
    public User(int id, String firstName, String lastName, String email, boolean enabled, boolean employer) {
        this.u_ID = id;
        this.u_FirstName = firstName;
        this.u_LastName = lastName;
        this.u_Email = email;
        this.u_Phone = "";
        this.u_Address = "";
        this.u_City = "";
        this.u_State = "";
        this.u_Zip = "";
        this.u_Employer = false;
        this.u_EmployerName = "";
        this.u_EmployerID = -1;
        this.u_Enabled = enabled;        
    }

    public void setID(int id) {
        this.u_ID = id;
    }
    
    public int getID() {
        return this.u_ID;
    }
    
    public void setEmail(String email) {
        this.u_Email = email;
    }
    
    public String getEmail() {
        return this.u_Email;
    }
    
    public void setFirstName(String firstName) {
        this.u_FirstName = firstName;
    }
    
    public String getFirstName() {
        return this.u_FirstName;
    }
    
    public void setLastName(String lastName) {
        this.u_LastName = lastName;
    }
    
    public String getLastName() {
        return this.u_LastName;
    }    
    
    public void setPhone(String phone) {
        this.u_Phone = phone;
    }
    
    public String getPhone() {
        return this.u_Phone;
    }
    
    public void setAddress(String address) {
        this.u_Address = address;
    }
    
    public String getAddress() {
        return this.u_Address;
    }
    
    public void setCity(String city) {
        this.u_City = city;
    }
    
    public String getCity() {
        return this.u_City;
    }
    
    public void setState(String state) {
        this.u_State = state;
    }
    
    public String getState() {
        return this.u_State;
    }
    
    public void setZip(String zip) {
        this.u_Zip = zip;
    }
    
    public String getZip() {
        return this.u_Zip;
    }
    
    public void setEmployer(boolean employer) {
        this.u_Employer = employer;
    }
    
    public boolean getEmployer() {
        return this.u_Employer;
    }
    
    public void setEmployerName(String employerName) {
        this.u_EmployerName = employerName;
    }
    
    public String getEmployerName() {
        return this.u_EmployerName;
    }
    
    public void setEmployerID(int eid) {
        this.u_EmployerID = eid;
    }
    
    public int getEmployerID() {
        return this.u_EmployerID;
    }
    
    public void setEnabled(boolean enabled) {
        this.u_Enabled = enabled;
    }
    
    public boolean getEnabled() {
        return this.u_Enabled;
    }
    
    @Override
    public String toString() {
        return  this.u_ID + "\n" + 
                this.u_Email + "\n" +
                this.u_FirstName + "\n" +
                this.u_LastName + "\n" +
                this.u_Phone + "\n" +
                this.u_Address + "\n" +
                this.u_City + "\n" +
                this.u_State + "\n" +
                this.u_Zip + "\n" +
                this.u_Employer + "\n" +
                this.u_EmployerName + "\n" +
                this.u_EmployerID + "\n" +
                this.u_Enabled  + "\n";
    }
}
