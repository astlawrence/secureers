/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static x.PasswordUtil.createHash;

/**
 *
 * @author astla
 */
public class ChangePassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "";
        String message = "";
        
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        
        // Get form parameters
        String password = request.getParameter("password");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");
        
        // Check if current password is correct
        if (UserDB.userExists(user.getEmail(), password)) {
            // Check if the auth code has been cleared, if not, user has not confirmed email address
            if (UserDB.getAuthCode(user.getEmail()).equalsIgnoreCase("null")) {
                // Check if user exists in DB
                if (UserDB.userExists(user.getEmail())) {
                        // Check if provided passwords match
                        if (password1.equals(password2)) {
                            url = "/profile.jsp";

                            String email = user.getEmail();

                            // Create a salt for the user
                            String salt = PasswordUtil.createSalt();

                            // Hash the user's password
                            String hash = null;
                            try {
                                hash = createHash(password1, salt);
                            } catch(PasswordUtil.CannotPerformOperationException e) {
                                System.err.println("ChangePassword - " + e);

                                message = "An error occurred while changing your password!";
                                url = "/error.jsp";
                            }

                            if (UserDB.changePassword(user, salt, hash)) {
                                url = "/profile.jsp";
                                message = "Password changed successfully!";

                                user = UserDB.getUser(email);
                            } else {
                                url = "/error.jsp"; 
                                message = "An error occurred while changing your password!";
                            }
                        } else {
                            message = "Passwords do not match!";
                            url = "/changepassword.jsp";
                        }
                } else {
                    url = "/changepassword.jsp";
                    message = "Password change failed!";
                }
            } else {
                url = "/changepassword.jsp";
                message = "Please confirm your email address!";
            }
        } else {
            url = "/changepassword.jsp";
            message = "Current password invalid!";
        }
        
        session.setAttribute("message", message);
        session.setAttribute("user", user);
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
