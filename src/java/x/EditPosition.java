/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author astla
 */
public class EditPosition extends HttpServlet {
    
    private static final String urlString = "http://localhost:8983/solr/secureers/dataimport?command=full-import";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        String message = "";
        String url = "";
        
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        Employer employer = (Employer) session.getAttribute("employer");
        Job[] jobs = (Job[]) session.getAttribute("jobs");
        
        String jid = request.getParameter("job");
        //System.out.println("Job ID: " + jid);
        
        String action = request.getParameter("action");
        
        if (action == null) {
            Job job = new Job();
            job = JobDB.getJob(Integer.parseInt(jid));
            //System.out.println(job.toString());
            
            url = "/postposition.jsp";
            
            session.setAttribute("job", job);
            session.setAttribute("jobs", jobs);
            session.setAttribute("employer", employer);
            session.setAttribute("user", user);
        } else if (action.equalsIgnoreCase("delete")) {
            //System.out.println("Delete called.");
            
            // Delete job from Job DB
            if (JobDB.deleteJob(Integer.parseInt(jid))) {
                URL solrURL = new URL(urlString);
                HttpURLConnection con = (HttpURLConnection) solrURL.openConnection();
                con.setRequestMethod("GET");
                int responseCode = con.getResponseCode();
                //System.out.println(responseCode);
                
                
                // Delete job from Employer DB
                if (EmployerDB.deleteJob(employer, Integer.parseInt(jid))) {
                    // Update employer
                    employer = EmployerDB.getEmployer(employer.getID());

                    if (employer.hasJobs()) {
                        // Get number of jobs employer hsa posted
                        int numberOfJobs = employer.numberOfJobs();

                        Job[] j = new Job[numberOfJobs];
                        UserProfile[] userProfiles = new UserProfile[numberOfJobs];
                        User[] users = new User[numberOfJobs];

                        for (int i = 0; i < numberOfJobs; i++) {
                            // Display jobs in reverse order of creation
                            int jobID = employer.getJob(employer.numberOfJobs() - i - 1);
                            //System.out.println("Login jobID: " + jid);
                            j[i] = JobDB.getJob(jobID);

                            // Select a random user from a group of users who best match the position skill requirements based on the tolerance
                            UserProfile up = UserProfileDB.searchUserProfileRandom(3, j[i].getSkills(), Employer.TOLERANCE);
                            if (!(up == null)) {
                                //System.out.println("Profile is not null.");
                                users[i] = UserDB.getUser(up.getUserID());
                            } else {
                                users[i] = new User();
                                //System.out.println("Null user");
                            }
                        }

                        url = "/profile.jsp";
                        
                        session.setAttribute("jobs", j);
                        session.setAttribute("users", users);
                        
                        session.removeAttribute("job");
                    } else {
                        message = "Unable to find positions associated with the employer!";
                        url = "/error.jsp";
                        
                        session.setAttribute("message", message);
                        session.removeAttribute("job");
                    }
                }

                session.setAttribute("employer", employer);
                session.setAttribute("user", user);
                session.removeAttribute("job");
            } else {
                message = "Unable to delete the position!";
                url = "/error.jsp";
                
                session.setAttribute("message", message);
            }
            session.removeAttribute("job");
        } else {
            message = "Unable to edit the position!";
            url = "/error.jsp";
            
            session.setAttribute("message", message);
            session.removeAttribute("job");
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
