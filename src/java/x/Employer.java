/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author astla
 */
public class Employer {
    
    public final static int EMPLOYEE_LIMIT = 10;
    public final static int JOBS_LIMIT = 20;
    public final static int JOBS_TO_DISPLAY = 3;
    public final static int TOLERANCE = 3;
    
    private int e_ID;
    private ArrayList<Integer> e_UserIDs;
    private String e_EmployerName;
    private ArrayList<Integer> e_Jobs;
    
    public Employer() {
        this.e_ID = -1;
        this.e_UserIDs = new ArrayList<Integer>(EMPLOYEE_LIMIT);
        this.e_Jobs = new ArrayList<Integer>(JOBS_LIMIT);
        this.e_EmployerName = "";
    }
    
    public Employer(String employerName) {
        this.e_ID = -1;
        this.e_UserIDs = new ArrayList<Integer>(EMPLOYEE_LIMIT);
        this.e_EmployerName = employerName;
        this.e_Jobs = new ArrayList<Integer>(JOBS_LIMIT);
    }
    
    public Employer(String employerName, Job[] jobs) {
        this.e_ID = -1;
        this.e_UserIDs = new ArrayList<Integer>(EMPLOYEE_LIMIT);
        this.e_EmployerName = employerName;
        
        for (int i = 0; i < Employer.JOBS_LIMIT; i++) {
            this.e_Jobs.add(jobs[i].getID());
        }
    }
    
    public Employer(String employerName, User[] users, Job[] jobs) {
        this.e_ID = -1;
        this.e_EmployerName = employerName;
        
        for (int i = 0; i < Employer.EMPLOYEE_LIMIT; i++) {
            this.e_UserIDs.add(users[i].getID());
        }
        
        for (int i = 0; i < Employer.JOBS_LIMIT; i++) {
            this.e_Jobs.add(jobs[i].getID());
        }
    }
    
    public void setID(int id) {
        this.e_ID = id;
    }
    
    public int getID() {
        return this.e_ID;
    }
    
    public void setEmployerName(String employerName) {
        this.e_EmployerName = employerName;
    }
    
    public String getEmployerName() {
        return this.e_EmployerName;
    }
    
    public void addUsers(User[] users) {
        this.e_UserIDs.clear();
        
        if (users.length < Employer.EMPLOYEE_LIMIT) {
            for (User user : users) {
                this.e_UserIDs.add(user.getID());
            }
        }
    }
    
    public ArrayList<Integer> getUsers() {
        return this.e_UserIDs;
    }
    
    public void removeUsers() {
        this.e_UserIDs.clear();
    }
    
    public boolean addUser(User user) {
        if (this.e_UserIDs.size() <= EMPLOYEE_LIMIT) {
            this.e_UserIDs.add(user.getID());
            
            return true;
        } else {
            return false;
        }
    }
    
    public boolean addUser(int uid) {
        if (this.e_UserIDs.size() <= EMPLOYEE_LIMIT) {
            this.e_UserIDs.add(uid);
            
            return true;
        } else {
            return false;
        }
    }
    
    public int getUser(int index) {
        if ((index >= 0) && (index < EMPLOYEE_LIMIT)) {
            return this.e_UserIDs.get(index);
        } else {
            return -1;
        }
    }
    
    public boolean removeUser(User user) {
        if (this.e_UserIDs.contains(user.getID())) {
            this.e_UserIDs.remove(user.getID());
            
            return true;
        } else {
            return false;
        }
    }
    
    public void addJobs(Job[] jobs) {
        this.e_Jobs.clear();
        
        for (Job job : jobs) {
            this.e_Jobs.add(job.getID());
        }
    }
    
    public ArrayList<Integer> getJobs() {
        return this.e_Jobs;
    }
    
    public void removeJobs() {
        this.e_Jobs.clear();
    }
    
    public boolean addJob(Job job) {
        if (this.e_Jobs.size() <= JOBS_LIMIT) {
            this.e_Jobs.add(job.getID());
            
            return true;
        } else {
            return false;
        }
    }
    
    public boolean addJob(int jid) {
        if (this.e_Jobs.size() <= JOBS_LIMIT) {
            this.e_Jobs.add(jid);
            
            return true;
        } else {
            return false;
        }
    }
    
    public int getJob(int index) {
        if (index < JOBS_LIMIT) {
            return this.e_Jobs.get(index);
        } else {
            return -1;
        }
    }
    
    public boolean removeJob(Job job) {
        if (this.e_Jobs.contains(job.getID())) {
            this.e_UserIDs.remove(job.getID());
            
            return true;
        } else {
            return false;
        }
    }
    
    public boolean hasJobs() {
        if (this.e_Jobs.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
    
    public int numberOfJobs() {
        return this.e_Jobs.size();
    }
    
    public int numberOfUsers() {
        return this.e_UserIDs.size();
    }
    
    @Override
    public String toString() {
        String users = "";
        for (int i = 0; i <= Employer.EMPLOYEE_LIMIT; i++) {
            if (i < this.e_UserIDs.size()) {
                users += this.e_UserIDs.get(i) + " ";
            }
        }
        
        String jobs = "";
        for (int i = 0; i <= Employer.JOBS_LIMIT; i++) {
            if (i < this.e_Jobs.size()) {
                jobs += this.e_Jobs.get(i) + " ";
            }
        }
        
        return this.e_ID + " " +
                this.e_EmployerName + " [" +
                users + "] [" +
                jobs + "]\n"; 
    }
}
