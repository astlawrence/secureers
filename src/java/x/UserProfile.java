/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.util.Arrays;

/**
 *
 * @author astla
 */
public class UserProfile {
    
    public final static int SKILLS_SIZE = 6;
    public final static int CAREER_SKILLS_SIZE = 6;
    public final static int JOBS_TO_DISPLAY = 3;
    public final static int TOLERANCE = 3;
    
    private int p_ID;
    private int p_UserID;
    private String p_Name;
    private int p_Skills[];
    private int p_CareerSkills[];
    
    public UserProfile() {
        int skills[] = new int[SKILLS_SIZE];
        Arrays.fill(skills, 0);
        
        int careerSkills[] = new int[CAREER_SKILLS_SIZE];
        Arrays.fill(careerSkills, 0);
        
        this.p_ID = -1;
        this.p_UserID = -1;
        this.p_Name = "";
        this.p_Skills = skills;
        this.p_CareerSkills = careerSkills;
    } 
    
    public UserProfile(int pid, int uid, String profileName, int[] skills, int[] careerSkills) {
        this.p_ID = pid;
        this.p_UserID = uid;
        this.p_Name = profileName;
        this.p_Skills = skills;
        this.p_CareerSkills = careerSkills;
    }
    
    public void setProfileID(int pid) {
        this.p_ID = pid;
    }
    
    public int getProfileID() {
        return this.p_ID;
    }
    
    public void setUserID(int uid) {
        this.p_UserID = uid;
    }
    
    public int getUserID() {
        return this.p_UserID;
    }
    
    public void setProfileName(String profileName) {
        this.p_Name = profileName;
    }
    
    public String getProfileName() {
        return this.p_Name;
    }
    
    public void setSkills(int[] skills) {
        this.p_Skills = skills;
    }
    
    public int[] getSkills() {
        return this.p_Skills;
    }
    
    public void setSkill(int index, int value) {
        this.p_Skills[index] = value;
    }
    
    public int getSkill(int index) {
        return this.p_Skills[index];
    }
    
    public void setCareerSkills(int[] careerSkills) {
        this.p_CareerSkills = careerSkills;
    }
    
    public int[] getCareerSkills() {
        return this.p_CareerSkills;
    }
    
    public void setCareerSkill(int index, int value) {
        this.p_CareerSkills[index] = value;
    }
    
    public int getCareerSkill(int index) {
        return this.p_CareerSkills[index];
    }
    
    @Override
    public String toString() {
        String skills = "";
        for (int i = 0; i < SKILLS_SIZE; i++) {
            skills += this.p_Skills[i] + " ";
        }
        
        String careerSkills = "";
        for (int i = 0; i < CAREER_SKILLS_SIZE; i++) {
            careerSkills += this.p_CareerSkills[i] + " ";
        }
        
        return this.p_ID + "\n" + this.p_UserID + "\n" + this.p_Name + "\n" + skills + "\n" + careerSkills;
    }
}
