/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author astla
 */
public class CareerSkills extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        String message = "";
        String url = "";
        
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
        Job[] jobs = (Job[]) session.getAttribute("jobs");
        
        // Get skill values from request
        int careerSkills[] = new int[UserProfile.CAREER_SKILLS_SIZE];
        for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
            String param = "careerskill" + Integer.toString(i + 1);
            String value = request.getParameter(param);
            careerSkills[i] = Integer.parseInt(value);
        }
        
        // Check if a user profile exists for the user
        if (UserProfileDB.checkUserProfileExists(user.getID())) {
            
            // Get the user profile
            userProfile = UserProfileDB.getUserProfile(user.getID());
            
            // Update the user profile with career skills
            userProfile.setCareerSkills(careerSkills);
            
            // Update the UserProfile DB with the career skills
            UserProfileDB.editUserProfileCareerSkills(userProfile);
             
            Job[] j = new Job[UserProfile.JOBS_TO_DISPLAY];
                        
            // Query Job DB for positions that match desired skills
            j = JobDB.searchJobs(UserProfile.JOBS_TO_DISPLAY, userProfile.getCareerSkills(), userProfile.TOLERANCE);
            
            url = "/profile.jsp";
            
            session.setAttribute("user", user);
            session.setAttribute("userProfile", userProfile);
            session.setAttribute("jobs", j);
        } else {
            // No user profile exists for the user, create a new one
            String profileName = "ProfileName";
            
            // Create a new user profile associated to the user
            if (UserProfileDB.addUserProfileCareerSkills(user.getID(), profileName, careerSkills)) {                
                url = "/profile.jsp";
                
                userProfile = UserProfileDB.getUserProfile(user.getID());
                
                Job[] j = new Job[UserProfile.JOBS_TO_DISPLAY];
                        
                // Query Job DB for positions that match desired skills
                j = JobDB.searchJobs(UserProfile.JOBS_TO_DISPLAY, userProfile.getCareerSkills(), userProfile.TOLERANCE);
                
                session.setAttribute("user", user);
                session.setAttribute("userProfile", userProfile);
                session.setAttribute("jobs", j);
            } else {
                message = "Unable to add user profile!";
                url = "/error.jsp";
                
                session.setAttribute("message", message);
            }    
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
