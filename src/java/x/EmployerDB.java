/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author astla
 */
public class EmployerDB {
    
    public static synchronized boolean checkExists(int eid) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT * FROM employer WHERE id = ?";
                
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, eid);
            rs = ps.executeQuery();
            if (rs.first()) { 
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println(e);
            
            return false;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean checkExists(Employer employer) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT * FROM employer WHERE id = ?";
                
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, employer.getID());
            rs = ps.executeQuery();
            if (rs.first()) { 
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println(e);
            
            return false;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized int checkUserAssociated(int uid) {
        // Check if a user ID is associated with any employer in the DB
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT * FROM employer WHERE userid";
        for (int i = 1; i < Employer.EMPLOYEE_LIMIT + 1; i++) {
            query += i + "= ?";
            if (i < Employer.EMPLOYEE_LIMIT) {
                query += " OR userid";
            }
        }
        query += ";";
                
        try {
            ps = connection.prepareStatement(query);
            
            for (int i = 1; i < Employer.EMPLOYEE_LIMIT + 1; i++) {
                ps.setInt(i, uid);
            }
            
            rs = ps.executeQuery();
            if (!rs.first()) { 
                // Advance cursor and check if a row exists, otherwise false.
                return -1;
            } else {
                return rs.getInt("ID");
            }
        } catch (SQLException e) {
            System.err.println(e);
            
            return -1;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized Employer getEmployer(int eid) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Employer employer = new Employer();
        
        String query = "SELECT * FROM employer WHERE id = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, eid);
            rs = ps.executeQuery();
            
            if (rs.first()) {
                employer.setID(eid);
                employer.setEmployerName(rs.getString("EMPNAME"));
                
                for (int i = 0; i < Employer.EMPLOYEE_LIMIT; i++) {
                    String param = "USERID" + Integer.toString(i + 1);
                    int uid = rs.getInt(param);
                    if (uid > 0) {
                        employer.addUser(uid);
                    }
                }
                
                for (int i = 0; i < Employer.JOBS_LIMIT; i++) {
                    String param = "JOB" + Integer.toString(i + 1);
                    int jid = rs.getInt(param);
                    if (jid > 0) {
                        //System.out.println("Added job ID " + jid);
                        employer.addJob(jid);
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("EmployerDB.getEmployer(int) - " + e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);    
        }
        
        return employer;
    }
    
    public static synchronized Employer getEmployer(int uid, String employerName) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Employer employer = new Employer();
        
        String query = "SELECT * FROM employer WHERE ";
        for (int i = 0; i < Employer.EMPLOYEE_LIMIT; i++) {
            query += " USERID" + Integer.toString(i + 1) + " = ?";
            if (i < Employer.EMPLOYEE_LIMIT - 1) {
                query += " OR";
            }
        }
        query += " AND EMPNAME = ?;";
        
        //System.out.println(query);
        
        try {
            ps = connection.prepareStatement(query);
            
            for (int i = 0; i < Employer.EMPLOYEE_LIMIT; i++) {
                ps.setInt(i + 1, uid);
            }
            
            ps.setString(Employer.EMPLOYEE_LIMIT + 1, employerName);
            
            rs = ps.executeQuery();
            
            if (rs.first()) {
                employer.setID(rs.getInt("ID"));
                employer.setEmployerName(rs.getString("EMPNAME"));
                
                for (int i = 0; i < Employer.EMPLOYEE_LIMIT; i++) {
                    String param = "USERID" + Integer.toString(i + 1);
                    int userid = rs.getInt(param);
                    if (userid > 0) {
                        employer.addUser(userid);
                    }
                }
                
                for (int i = 0; i < Employer.JOBS_LIMIT; i++) {
                    String param = "JOB" + Integer.toString(i + 1);
                    int jid = rs.getInt(param);
                    if (jid > 0) {
                        employer.addJob(jid);
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println("EmployerDB.getEmployer(int) - " + e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);    
        }
        
        return employer;
    }
    
    public static synchronized boolean addEmployer(Employer employer) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
                
        String insert = "INSERT INTO employer (EMPNAME, USERID1) " +
                "VALUES(?, ?);";
                       
        try {
            ps = connection.prepareStatement(insert);
            ps.setString(1, employer.getEmployerName());
            ps.setInt(2, employer.getUser(0));
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("EmployerDB.addEmployer(Employer) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean addJob(Employer employer, Job job) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        int i = employer.numberOfJobs();
        //System.out.println("EmployerDB.addJob(employer, job) Number of jobs: " + i);
        
        //System.out.println("Employer ID: " + employer.getID());
        //System.out.println("Job ID: " + job.getID());
        
        String update = "UPDATE employer SET JOB" + i + "=? WHERE id=?;";
                       
        try {
            ps = connection.prepareStatement(update);
            ps.setInt(1, job.getID());
            ps.setInt(2, employer.getID());
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("EmployerDB.addJob(Employer, Job) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean deleteJob(Employer employer, int jid) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT * FROM employer WHERE id = ?";
        String update = "UPDATE employer SET JOB";
        
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, employer.getID());
            rs = ps.executeQuery();
            
            // An employer record was found
            if (rs.first()) {
                //System.out.println("Number of jobs: " + employer.numberOfJobs());
                
                // Get the job IDs associated with the employer
                ArrayList<Integer> jobs = new ArrayList<Integer>(Employer.JOBS_LIMIT);
                for (int i = 0; i < employer.numberOfJobs(); i++) {
                    jobs.add(rs.getInt("JOB" + Integer.toString(i + 1)));
                }
                
                // Complete the update query with the correct Employer DB column matching the job ID
                if (jobs.contains(jid)) {
                    //System.out.println("Job ID: " + jid);
                    update += (jobs.indexOf(jid) + 1) + " = NULL WHERE id = ?";
                    //System.out.println("Update: " + update);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println("EmployerDB.deleteJob(Employer, int) - " + e);
            
            return false;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }

        ConnectionPool pool2 = ConnectionPool.getInstance();
        Connection connection2 = pool2.getConnection();
        PreparedStatement ps2 = null;
        
        try {
            ps2 = connection2.prepareStatement(update);
            ps2.setInt(1, employer.getID());
            ps2.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("EmployerDB.deleteJob(int, int) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps2);
            pool.freeConnection(connection2);
        }        
    }
}
