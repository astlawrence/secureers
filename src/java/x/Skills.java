/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author astla
 */
public class Skills extends HttpServlet {

    private static final String urlString = "http://localhost:8983/solr/candidates/dataimport?command=full-import";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        String message = "";
        String url = "";
        
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
        
        // Get skill values from request
        int skills[] = new int[UserProfile.SKILLS_SIZE];
        for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
            String param = "skill" + Integer.toString(i + 1);
            String value = request.getParameter(param);
            skills[i] = Integer.parseInt(value);
        }
        
        if (UserProfileDB.checkUserProfileExists(user.getID())) {
            
            userProfile = UserProfileDB.getUserProfile(user.getID());
            
            userProfile.setSkills(skills);
            
            UserProfileDB.editUserProfileSkills(userProfile);
            
            userProfile = UserProfileDB.getUserProfile(user.getID());
            
            // Update solr
            URL solrURL = new URL(urlString);
            HttpURLConnection con = (HttpURLConnection) solrURL.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            
            url = "/profile.jsp";
            
            session.setAttribute("user", user);
            session.setAttribute("userProfile", userProfile);
        } else {
            String profileName = "ProfileName";
            
            // Create a new user profile associated to the user
            if (UserProfileDB.addUserProfileSkills(user.getID(), profileName, skills)) {
                URL solrURL = new URL(urlString);
                HttpURLConnection con = (HttpURLConnection) solrURL.openConnection();
                con.setRequestMethod("GET");
                int responseCode = con.getResponseCode();
                
                url = "/profile.jsp";
                
                userProfile = UserProfileDB.getUserProfile(user.getID());
                                
                session.setAttribute("user", user);
                session.setAttribute("userProfile", userProfile);
                
            } else {
                message = "Unable to add user profile!";     
                url = "/error.jsp";
                
                session.setAttribute("message", message);
            }    
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
