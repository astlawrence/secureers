/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author astla
 */
public class PostPosition extends HttpServlet {

    private static final String urlString = "http://localhost:8983/solr/secureers/dataimport?command=full-import";
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        String message = "";
        String url = "";
        
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        Employer employer = (Employer) session.getAttribute("employer");
        Job[] jobs = (Job[]) session.getAttribute("jobs");
        Job jobEdit = (Job) session.getAttribute("job");
        
        //System.out.println(jobEdit);
        //System.out.println(user);
        
        if (jobEdit == null) {
            System.out.println("jobEdit is null.");
            // New position to post
            Job job = new Job();
            //user.toString();
            
            job.setUserID(user.getID());
            job.setEmployerID(employer.getID());
            job.setTitle(request.getParameter("title"));
            job.setSummary(request.getParameter("summary"));
            job.setEmploymentType(request.getParameter("employmentType"));
            job.setSalary(request.getParameter("salary"));
            job.setYearsExperience(request.getParameter("yearsExperience"));
            job.setEducation(request.getParameter("education"));
            job.setCity(request.getParameter("city"));
            job.setState(request.getParameter("state"));
            job.setZip(request.getParameter("zip"));

            String[] responsibilities = new String[Job.RESPONSIBILITIES_SIZE];
            for (int i = 0; i < Job.RESPONSIBILITIES_SIZE; i++) {
                String param = "responsibility" + Integer.toString(i + 1);
                responsibilities[i] = request.getParameter(param);
            }
            job.setResponsibilities(responsibilities);

            int[] skills = new int[Job.SKILLS_SIZE];
            for (int i = 0; i < Job.SKILLS_SIZE; i++) {
                String param = "skill" + Integer.toString(i + 1);
                String x = request.getParameter(param);
                skills[i] = Integer.parseInt(x);
            }
            job.setSkills(skills);

            // Check if employer has an available job slot opening
            if (employer.numberOfJobs() <= Employer.JOBS_LIMIT) {
                // Add job to job DB
                if (JobDB.addJob(job)) {
                    // Get added job ID and associate it to the employer
                    int jid = JobDB.getJobID(job);
                    //System.out.println("PostPosition job ID: " + jid);

                    // If the job ID is not -1 then the job was found in the job DB
                    if (jid != -1) {
                        url = "/profile.jsp";

                        // Set the job ID and add the job to the employer object
                        job.setID(jid);

                        // Associate the job with the employer
                        employer.addJob(job);

                        // Update the employer DB with the job
                        EmployerDB.addJob(employer, job);

                        // Update employer object
                        employer = EmployerDB.getEmployer(employer.getID());
                        
                        // Update solr
                        URL solrURL = new URL(urlString);
                        HttpURLConnection con = (HttpURLConnection) solrURL.openConnection();
                        con.setRequestMethod("GET");
                        int responseCode = con.getResponseCode();
                    } else {
                        message = "Unable to add job!";
                        url = "/error.jsp";

                        session.setAttribute("message", message);
                    }
                } else {
                    message = "Unable to add job!";
                    url = "/error.jsp";

                    session.setAttribute("message", message);
                }
            } else {
                message = "Unable to post new position! The maximum number of posted positions has been reached!";
                url = "/error.jsp";

                session.setAttribute("message", message);
            }

            if (employer.hasJobs()) {
                // Get number of jobs employer hsa posted
                int numberOfJobs = employer.numberOfJobs();

                Job[] j = new Job[numberOfJobs];
                UserProfile[] userProfiles = new UserProfile[numberOfJobs];
                User[] users = new User[numberOfJobs];

                for (int i = 0; i < numberOfJobs; i++) {
                    // Display jobs in reverse order of creation
                    int jid = employer.getJob(employer.numberOfJobs() - i - 1);
                    //System.out.println("Login jobID: " + jid);
                    j[i] = JobDB.getJob(jid);

                    // Select a random user from a group of users who best match the position skill requirements based on the tolerance
                    UserProfile up = UserProfileDB.searchUserProfileRandom(3, j[i].getSkills(), Employer.TOLERANCE);
                    if (!(up == null)) {
                        //System.out.println("Profile is not null.");
                        users[i] = UserDB.getUser(up.getUserID());
                    } else {
                        users[i] = new User();
                        //System.out.println("Null user");
                    }
                }

                session.setAttribute("jobs", j);
                session.setAttribute("users", users);
            }

            session.setAttribute("employer", employer);
            session.setAttribute("user", user);
        } else {
            // Job edit, check if the job exists in Job DB
            if (JobDB.jobExists(jobEdit)) {
                // Update the job if it exists
                //System.out.println("-------------\nJob exists.\n-------------");
                Job job = new Job();
                job = JobDB.getJob(jobEdit);
                
                job.setTitle(request.getParameter("title"));
                job.setSummary(request.getParameter("summary"));
                job.setEmploymentType(request.getParameter("employmentType"));
                job.setSalary(request.getParameter("salary"));
                job.setYearsExperience(request.getParameter("yearsExperience"));
                job.setEducation(request.getParameter("education"));
                job.setCity(request.getParameter("city"));
                job.setState(request.getParameter("state"));
                job.setZip(request.getParameter("zip"));

                String[] responsibilities = new String[Job.RESPONSIBILITIES_SIZE];
                for (int i = 0; i < Job.RESPONSIBILITIES_SIZE; i++) {
                    String param = "responsibility" + Integer.toString(i + 1);
                    responsibilities[i] = request.getParameter(param);
                }
                job.setResponsibilities(responsibilities);

                int[] skills = new int[Job.SKILLS_SIZE];
                for (int i = 0; i < Job.SKILLS_SIZE; i++) {
                    String param = "skill" + Integer.toString(i + 1);
                    String x = request.getParameter(param);
                    skills[i] = Integer.parseInt(x);
                }
                job.setSkills(skills);
                
                if (JobDB.updateJob(job)) {
                    //System.out.println(JobDB.getJob(job).toString());
                    URL solrURL = new URL(urlString);
                    HttpURLConnection con = (HttpURLConnection) solrURL.openConnection();
                    con.setRequestMethod("GET");
                    int responseCode = con.getResponseCode();
                    //System.out.println(responseCode);
                    
                    // Fetch updated jobs data for the employer
                    if (employer.hasJobs()) {
                        // Get number of jobs employer hsa posted
                        int numberOfJobs = employer.numberOfJobs();

                        Job[] j = new Job[numberOfJobs];
                        UserProfile[] userProfiles = new UserProfile[numberOfJobs];
                        User[] users = new User[numberOfJobs];

                        for (int i = 0; i < numberOfJobs; i++) {
                            // Display jobs in reverse order of creation
                            int jid = employer.getJob(employer.numberOfJobs() - i - 1);
                            j[i] = JobDB.getJob(jid);
                            
                            // Select a random user from a group of users who best match the position skill requirements based on the tolerance
                            UserProfile up = UserProfileDB.searchUserProfileRandom(3, j[i].getSkills(), Employer.TOLERANCE);
                            if (!(up == null)) {
                                users[i] = UserDB.getUser(up.getUserID());
                            } else {
                                users[i] = new User();
                            }
                        }
                        url = "/profile.jsp";

                        session.setAttribute("jobs", j);
                        session.setAttribute("users", users);
                    }

                    session.setAttribute("employer", employer);
                    session.setAttribute("user", user);
                    
                    session.removeAttribute("job");
                } else {
                    message = "Unable to edit position!";
                    url = "/error.jsp";
                    
                    session.setAttribute("message", message);
                    session.removeAttribute("job");
                }
            } else {
                message = "Unable to edit position! Position does not exist!";
                url = "/error.jsp";
                
                session.setAttribute("message", message);
                session.removeAttribute("job");
            }
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
