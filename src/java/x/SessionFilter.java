/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author astla
 */
public class SessionFilter implements Filter {
    private FilterConfig filterConfig = null;
    private ArrayList<String> urlList;
    
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        
        String urls = filterConfig.getInitParameter("avoid-urls");
        StringTokenizer token = new StringTokenizer(urls, ",");

	urlList = new ArrayList<String>();

	while (token.hasMoreTokens()) {
            urlList.add(token.nextToken());
        }
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        
        HttpSession session = request.getSession(false);
        
        String url = request.getServletPath();
        
        if (urlList.contains(url) || url.contains("assets")) {
            chain.doFilter(req, res);
        } else {
            if (session == null) {
                response.sendRedirect("/x/login.jsp");
            } else {
                chain.doFilter(req, res);
            }
        }
    }

    @Override
    public void destroy() {
        filterConfig = null;
    }
}
