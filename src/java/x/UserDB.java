/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.sql.*;
import static x.PasswordUtil.createHash;
/**
 *
 * @author astla
 */
public class UserDB {
    
    public static synchronized boolean userExists(String email) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        // Check if user is enabled
        String query = "SELECT * FROM user WHERE email = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            
            if (!rs.first()) { 
                return false;
            } else { 
                return true;
            }
        } catch (SQLException e) {
            System.err.println("UserDB.checkExists(String) - " + e);
                       
            return false;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean userExists(String email, String pass) {
        // Check if a user exists and has provided the correct credentials,
        // otherwise return false.
        // Get the stored salt of the user
        // Get the stored password hash of the user
        // Compute a hash based on the provided password and salt
        // Compare computed hash with stored hash
        
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String salt = null; 
            // Stored DB salt
        String hash = null;
            // Computed hash
        String password = null;
            // Stored DB hash
        boolean enabled = false;
        
        // Get the salt and if user is enabled
        String query = "SELECT salt,enabled FROM user WHERE email = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (!rs.first()) { 
                // Advance cursor and check if a row exists, otherwise false.
                return false;
            }
            
            salt = rs.getString("SALT");
            enabled = rs.getBoolean("ENABLED");
        } catch (SQLException e) {
            System.err.println(e);
            
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
            
            return false;
        } 
        
        // Get user hash
        query = "SELECT pass FROM user WHERE email = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            if (!rs.first()) { 
                // Advance cursor and check if a row exists, otherwise false.
                return false;
            }
            
            password = rs.getString("PASS");
        } catch (SQLException e) {
            System.err.println("UserDB.userExists(String, String) - " + e);
            
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
            
            return false;
        } 
        
        // Compute password hash
        try {
            hash = PasswordUtil.createHash(pass, salt);
        } catch(PasswordUtil.CannotPerformOperationException e) {
            System.err.println(e);
            
            return false;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        // Compare computed hash with stored hash and if user is enabled
        if (password.equals(hash)) {
            if (enabled) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static synchronized User getUser(String email) {
        // Pre-condition: user exists
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        User user = new User();
        
        String query = "SELECT * FROM user WHERE email = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            
            if (rs.first()) {
                user.setID(rs.getInt("ID"));
                user.setEmail(rs.getString("EMAIL"));
                user.setFirstName(rs.getString("FNAME"));
                user.setLastName(rs.getString("LNAME"));
                user.setPhone(rs.getString("PHONE"));
                user.setAddress(rs.getString("ADDRESS"));
                user.setCity(rs.getString("CITY"));
                user.setState(rs.getString("STATE"));
                user.setZip(rs.getString("ZIP"));
                user.setEmployer(rs.getBoolean("EMPLOYER"));
                user.setEmployerName(rs.getString("EMPLOYERNAME"));
                user.setEmployerID(rs.getInt("EMPLOYERID"));
                user.setEnabled(rs.getBoolean("ENABLED"));
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        return user;
    }
    
    public static synchronized User getUser(User user) {
        // Pre-condition: user exists
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String email = user.getEmail();
        
        String query = "SELECT * FROM user WHERE email = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            rs.first();
            
            user.setID(rs.getInt("ID"));
            user.setEmail(rs.getString("EMAIL"));
            user.setFirstName(rs.getString("FNAME"));
            user.setLastName(rs.getString("LNAME"));
            user.setPhone(rs.getString("PHONE"));
            user.setAddress(rs.getString("ADDRESS"));
            user.setCity(rs.getString("CITY"));
            user.setState(rs.getString("STATE"));
            user.setZip(rs.getString("ZIP"));
            user.setEmployer(rs.getBoolean("EMPLOYER"));
            user.setEmployerName(rs.getString("EMPLOYERNAME"));
            user.setEmployerID(rs.getInt("EMPLOYERID"));
            user.setEnabled(rs.getBoolean("ENABLED"));
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        return user;
    }
    
    public static synchronized User getUser(int uid) {
        // Pre-condition: user exists
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        User user = new User();
        
        String query = "SELECT * FROM user WHERE id = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, uid);
            rs = ps.executeQuery();
            
            if (rs.first()) {
                user.setID(rs.getInt("ID"));
                user.setEmail(rs.getString("EMAIL"));
                user.setFirstName(rs.getString("FNAME"));
                user.setLastName(rs.getString("LNAME"));
                user.setPhone(rs.getString("PHONE"));
                user.setAddress(rs.getString("ADDRESS"));
                user.setCity(rs.getString("CITY"));
                user.setState(rs.getString("STATE"));
                user.setZip(rs.getString("ZIP"));
                user.setEmployer(rs.getBoolean("EMPLOYER"));
                user.setEmployerName(rs.getString("EMPLOYERNAME"));
                user.setEmployerID(rs.getInt("EMPLOYERID"));
                user.setEnabled(rs.getBoolean("ENABLED"));

                return user;
            } else {
                return null;
            }
        } catch (SQLException e) {
            System.err.println(e);
            
            return null;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean updateUser(User user) {
        // Pre-condition: user exists
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        // Build update statement
        String update = "UPDATE user SET FNAME='" + user.getFirstName() +
                "',LNAME='" + user.getLastName() + 
                "',PHONE='" + user.getPhone() +
                "',ADDRESS='" + user.getAddress() +
                "',CITY='" + user.getCity() +
                "',STATE='" + user.getState() + 
                "',ZIP='" + user.getZip() +
                "',EMPLOYERNAME='" + user.getEmployerName() +
                "',EMPLOYERID='" + user.getEmployerID() +
                "' WHERE id=" + user.getID() + ";";

        try {
            ps = connection.prepareStatement(update);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("UserDB.updateUser(User) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean changePassword(User user, String salt, String hash) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String update = "UPDATE user SET SALT = ?, HASH = ? WHERE id = ? AND email = ?";
        
        try {
            ps = connection.prepareStatement(update);
            ps.setString(1, salt);
            ps.setString(2, hash);
            ps.setInt(3, user.getID());
            ps.setString(4, user.getEmail());
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("RegisterDB.registerUser() : " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized String getAuthCode(String email) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String code = "";
        
        String query = "SELECT authcode FROM user WHERE email = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            rs.first();
            
            code = rs.getString("AUTHCODE");
        } catch (SQLException e) {
            System.err.println("UserDB.getAuthCode() : " + e);
            
            return "null";
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        return code;
    }
    
    public static synchronized boolean clearAuthCode(String email) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String update = "UPDATE user SET AUTHCODE='NULL' WHERE EMAIL=?;";
        try {
            ps = connection.prepareStatement(update);
            ps.setString(1, email);
            ps.executeUpdate();

            return true;
        } catch (SQLException e) {
            System.err.println("UserDB.clearAuthCode(String) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean clearSalt(String email) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String update = "UPDATE user SET SALT='NULL' WHERE EMAIL=?;";
        try {
            ps = connection.prepareStatement(update);
            ps.setString(1, email);
            ps.executeUpdate();

            return true;
        } catch (SQLException e) {
            System.err.println("UserDB.clearSalt(String) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean getEnabled(String email) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        boolean enabled = false;
        
        String query = "SELECT authcode FROM user WHERE email = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            rs.first();
            
            enabled = rs.getBoolean("ENABLED");
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        return enabled;
    }
    
    public static synchronized boolean setEnabled(String email) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String update = "UPDATE user SET enabled='1' WHERE email = ?";
        try {
            ps = connection.prepareStatement(update);
            ps.setString(1, email);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        return false;
    }
    
    public static synchronized boolean isEmployer(String email) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        boolean employer = false;
        
        String query = "SELECT employer FROM user WHERE email = ?";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, email);
            rs = ps.executeQuery();
            rs.first();
            
            employer = rs.getBoolean("EMPLOYER");
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        return employer;
    }
}
