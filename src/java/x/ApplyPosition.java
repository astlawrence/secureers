/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author astla
 */
public class ApplyPosition extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        HttpSession session = request.getSession(false);
        String jobID = request.getParameter("job");
        User user = (User) session.getAttribute("user");
        
        String url = "";
        
        // Get position details
        Job job = JobDB.getJob(Integer.parseInt(jobID));
        
        // Get employer info
        Employer employer = EmployerDB.getEmployer(job.getEmployerID());
        
        // Get user who posted the position
        int postingID = job.getUserID();
        User postingUser = UserDB.getUser(postingID);
        
        // Build email
        String subject = "New application for " + job.getTitle() + "!";
        String messageBody = "<html>" + postingUser.getFirstName() + ", <br /><br />" + user.getFirstName() + " " + 
                user.getLastName() + " applied for the open " + job.getTitle() + "!</html>";
            
        try {
            EmailUtil.sendEmail("astlawr1.jhu@gmail.com", postingUser.getEmail(), subject, messageBody, true);
            
            String applyMessage = "Successfully applied for " + job.getTitle() + "!";
            url = "/profile.jsp";
        
            session.setAttribute("applyMessage", applyMessage);
        } catch(MessagingException e) {
            System.err.println("ApplyPosition - " + e);
                
            String message = "Unable to send application!";
            url = "/error.jsp";
                
            session.setAttribute("message", message);
        }
               
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
