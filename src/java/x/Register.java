/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static x.PasswordUtil.createHash;

/**
 *
 * @author astla
 */
public class Register extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        
        String url = "";
        
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String password = request.getParameter("password1");
        String employer = request.getParameter("employer");
        
        // Create a salt for the user
        String salt = PasswordUtil.createSalt();
        
        // Hash the user's password
        String hash = null;
        try {
            hash = createHash(password, salt);
        } catch(PasswordUtil.CannotPerformOperationException e) {
            System.err.println("Register - processRequest()\n\n" + e);
            //url = "ERROR.JSP";
        }
        
        // Generate registration auth code
        SecureRandom random = new SecureRandom();
        String code = new BigInteger(130, random).toString(32);
        
        // Add the user to the database
        if (RegisterDB.registerUser(email, hash, salt, code, firstName, lastName, employer)) {            
            String subject = "Confirmation Email";
            String message = "<html>You registered for an account.  Please click the following link to confirm your registration.<br /><br />" +
                    "Confirmation: <a href=\"http://localhost:8084/x/register?email=" + email + 
                    "&code=" + code + "\">http://localhost:8084/x/register?email=" + email + "&code=" + code + "</a></html>";
            
            try {
                EmailUtil.sendEmail("astlawr1.jhu@gmail.com", email, subject, message, true);
            } catch(MessagingException e) {
                System.err.println("Email failed - " + e);
                //url = "ERROR.JSP";
            }
            
            url = "/email.jsp";
            
            session.setAttribute("email", email);
        } else {
            String message = "Unable to send confirmation email!";
            url = "/error.jsp";
            
            session.setAttribute("message", message);
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String email = request.getParameter("email");
        String authCode = request.getParameter("code");
        
        HttpSession session = request.getSession();
        
        String url = "";
        String message = "";
        
        String code = UserDB.getAuthCode(email);
        
        if (authCode.equals(code)) {
            if (UserDB.setEnabled(email)) {
                url = "/login.jsp";
                message = "Email confirmation successful, please login!";
                
                UserDB.clearAuthCode(email);
                
                session.setAttribute("message", message);
            } else {
                // url = "ERROR.jsp";
            }
        } else {
            // url = "ERROR.jsp";
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
