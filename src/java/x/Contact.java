package x;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author astla
 */
public class Contact extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        String message = "";
        String url = "";
        
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        Employer employer = (Employer) session.getAttribute("employer");
        
        String uid = request.getParameter("uid");
        String jid = request.getParameter("job");
        
        User u = UserDB.getUser(Integer.parseInt(uid));
        
        String subject = "";
        String messageBody = "";
        
        if (!(employer == null)) {
            // Employer user to normal user about a position
            subject = employer.getEmployerName() + " is interested in your profile!";
            
            if (jid == null) {
                // Employer to normal user solicitation
                messageBody = "<html>" + u.getFirstName() + ", <br /><br />" + employer.getEmployerName() + " is interested in you for one of their positions!</html>";
            } else {
                // Employer to normal user about a specific position
                Job j = JobDB.getJob(Integer.parseInt(jid));
                messageBody = "<html>" + u.getFirstName() + ", <br /><br />" + employer.getEmployerName() + " is interested in you for their " + j.getTitle() +" position!</html>";
            }
            
            try {
                EmailUtil.sendEmail("astlawr1.jhu@gmail.com", u.getEmail(), subject, messageBody, true);

                String applyMessage = "Successfully contacted " + u.getFirstName() + " " + u.getLastName() + "!";
                url = "/profile.jsp";

                session.setAttribute("applyMessage", applyMessage);
            } catch(MessagingException e) {
                System.err.println("Contact - " + e);

                message = "Unable to send contact email!";
                url = "/error.jsp";

                session.setAttribute("message", message);
            }
        } else {
            message = "An error occurred!";
            url = "/error.jsp";
            
            session.setAttribute("message", message);
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
