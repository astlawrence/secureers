package x;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author astla
 */
public class Info extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        String message = "";
        String url = "";
        
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
        Employer employer = (Employer) session.getAttribute("employer");
        Job[] jobs = (Job[]) session.getAttribute("jobs");
        
        String email = user.getEmail();
        
        user.setFirstName(request.getParameter("firstName"));
        user.setLastName(request.getParameter("lastName"));
        user.setPhone(request.getParameter("phone"));
        user.setAddress(request.getParameter("address"));
        user.setCity(request.getParameter("city"));
        user.setState(request.getParameter("state"));
        user.setZip(request.getParameter("zip"));
        
        // User claims to be associated with an employer, get employer name and ID from user
        if (user.getEmployer() == true) {
            user.setEmployerName(request.getParameter("employerName"));
            user.setEmployerID(Integer.parseInt(request.getParameter("employerID")));
            
            //System.out.println("Info - Employer ID: " + employer.getID());
            // Check if employer exists - employer object set during login
            if (EmployerDB.checkExists(employer)) {
                // Check if user is associated with the employer
                //System.out.println("Info - Employer exists.");
                
                // User is associated with the employer
                employer = EmployerDB.getEmployer(user.getEmployerID());
            } else {
                // Employer does not exist, create new employer
                //System.out.println("Info - Employer does not exist.");
                Employer e = new Employer();
                e.setEmployerName(request.getParameter("employerName"));
                e.addUser(user);
                
                EmployerDB.addEmployer(e);
                
                employer = EmployerDB.getEmployer(user.getID(), user.getEmployerName());
                
                UserDB.updateUser(user);
                
                user = UserDB.getUser(email);
            }
            
            url = "/profile.jsp";
            session.setAttribute("user", user);
            session.setAttribute("employer", employer);
            session.setAttribute("jobs", jobs);
        } else {
            // User is not associated with an employer, update.
            if (UserDB.updateUser(user)) {
                user = UserDB.getUser(user);

                url = "/profile.jsp";

                session.setAttribute("user", user);
                session.setAttribute("userProfile", userProfile);
            } else {
                message = "Error updating user information!";
                url = "/error.jsp";
                
                session.setAttribute("message", message);
            }
        }
        
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
