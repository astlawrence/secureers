/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.sql.*;

/**
 *
 * @author astla
 */
public class RegisterDB {
    
    public static boolean registerUser(String email, String password, String salt, String code, String firstName, String lastName, String employer) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String insert = "INSERT INTO user (EMAIL, PASS, SALT, AUTHCODE, FNAME, LNAME, EMPLOYER, EMPLOYERID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        
        try {
            ps = connection.prepareStatement(insert);
            ps.setString(1, email);
            ps.setString(2, password);
            ps.setString(3, salt);
            ps.setString(4, code);
            ps.setString(5, firstName);
            ps.setString(6, lastName);
            ps.setInt(7, Integer.parseInt(employer));
            ps.setInt(8, -1);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("RegisterDB.registerUser() : " + e);
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
}
