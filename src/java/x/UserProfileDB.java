/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

/**
 *
 * @author astla
 */
public class UserProfileDB {
    
    public static synchronized boolean addUserProfileSkills(int uid, String profileName, int[] skills) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        // Build insert statement
        String insert = "INSERT INTO profile (USERID, PROFILENAME";
        for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
            insert += ", SKILL" + (i + 1);
        }
        insert += ") VALUES (?, ?";
        for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
            insert += ", ?";
        }
        insert += ")";
        
        try {
            ps = connection.prepareStatement(insert);
            ps.setInt(1, uid);
            ps.setString(2, profileName);
            
            for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
                ps.setInt((i + 3) , skills[i]);
            }
            
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("UserProfileDB.addUserProfileSkills(int, String, int[]) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }    
    }
    
    public static synchronized boolean addUserProfileCareerSkills(int uid, String profileName, int[] careerSkills) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        // Build insert statement
        String insert = "INSERT INTO profile (USERID, PROFILENAME";
        for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
            insert += ", CSKILL" + (i + 1);
        }
        insert += ") VALUES (?, ?";
        for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
            insert += ", ?";
        }
        insert += ")";
        
        try {
            ps = connection.prepareStatement(insert);
            ps.setInt(1, uid);
            ps.setString(2, profileName);
            
            for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                ps.setInt((i + 3) , careerSkills[i]);
            }
            
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("UserProfileDB.addUserCareerProfileSkills(int, String, int[]) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }    
    }
    
    public static synchronized UserProfile getUserProfile(int uid) {
        // Pre-condition: user exists and user profile exists
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        UserProfile userProfile = new UserProfile();
        
        String query = "SELECT * FROM profile WHERE userid = ?";
        
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, uid);
            rs = ps.executeQuery();
            
            if (rs.first()) {
                userProfile.setProfileID(rs.getInt("ID"));
                userProfile.setUserID(uid);
                userProfile.setProfileName(rs.getString("PROFILENAME"));
                
                int skills[] = new int[UserProfile.SKILLS_SIZE];
                for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
                    String col = "SKILL" + Integer.toString(1 + i);
                    skills[i] = rs.getInt(col);
                }
                userProfile.setSkills(skills);
                
                int careerSkills[] = new int[UserProfile.CAREER_SKILLS_SIZE];
                for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                    String col = "CSKILL" + Integer.toString(1 + i);
                    careerSkills[i] = rs.getInt(col);
                }
                userProfile.setCareerSkills(careerSkills);
            }            
        } catch (SQLException e) {
            System.err.println("UserProfileDB.getUserProfile " + e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        return userProfile;
    }
    
    public static synchronized boolean checkUserProfileExists(int uid) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String query = "SELECT * FROM profile WHERE userid = ?";
        
        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, uid);
            rs = ps.executeQuery();
            if (rs.first()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.err.println(e);
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        return false;
    }
    
    public static synchronized boolean editUserProfileSkill(int pid, int index, int value) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String insert = "INSERT INTO profile (SKILL" + index + ") VALUES (?) WHERE ID = " + pid;
        
        try {
            ps = connection.prepareStatement(insert);
            ps.setInt(1, value);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("UserProfileDB.editUserProfileSkill(int, int int) - " + e);
           
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean editUserProfileSkills(UserProfile userProfile) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        // Build update statement
        String update = "UPDATE profile SET ";
        for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
            update += "SKILL" + (i + 1) + "=" + userProfile.getSkill(i);
            if (i < (UserProfile.SKILLS_SIZE - 1)) {
                update += ",";
            }
        }
        update += " WHERE id=" + userProfile.getProfileID() + ";";
        
        try {
            ps = connection.prepareStatement(update);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("UserProfileDB.editUserProfileSkills(UserProfile) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean editUserProfileCareerSkill(int pid, int index, int value) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        String insert = "INSERT INTO profile (CSKILL" + index + ") VALUES (?) WHERE ID = " + pid;
        
        try {
            ps = connection.prepareStatement(insert);
            ps.setInt(1, value);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("UserProfileDB.editUserProfileCareerSkills(int, int, int) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized boolean editUserProfileCareerSkills(UserProfile userProfile) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        
        // Build update statement
        String update = "UPDATE profile SET ";
        for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
            update += "CSKILL" + (i + 1) + "=" + userProfile.getCareerSkill(i);
            if (i < (UserProfile.CAREER_SKILLS_SIZE - 1)) {
                update += ",";
            }
        }
        update += " WHERE id=" + userProfile.getProfileID() + ";";
        
        try {
            ps = connection.prepareStatement(update);
            ps.executeUpdate();
            
            return true;
        } catch (SQLException e) {
            System.err.println("UserProfileDB.editUserProfileCareerSkills(UserProfile) - " + e);
            
            return false;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
    
    public static synchronized UserProfile[] searchUserProfiles(int numberOfUserProfiles, int[] skills, int tolerance) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        int count = 0;
        int userProfilesCounter = 0;
        UserProfile[] userProfiles = new UserProfile[numberOfUserProfiles];
        
        // Prepare the query to find out if any jobs exist in the Job DB that match the user's desired skills
        String queryCount = "SELECT COUNT(*) AS rows FROM profile WHERE (";
        for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
            queryCount += "SKILL" + Integer.toString(i + 1) + " BETWEEN ? AND ?)";
            if (i < UserProfile.SKILLS_SIZE - 1) {
                queryCount += " AND (";
            }
        }
       
        try {
            ps = connection.prepareStatement(queryCount);
            
            for (int i = 1; i < UserProfile.SKILLS_SIZE + 1; i++) {
                ps.setInt((2*i) - 1, skills[i - 1] - tolerance);
                ps.setInt((2*i), skills[i - 1] + tolerance);
            }
            
            rs = ps.executeQuery();
            
            if (rs.first()) {                
                // At least one job was found matching the user's desired career skills
                count = rs.getInt("rows");            
            } else {
                return null;
            }
        } catch (SQLException e) {
            System.err.println("UserProfile.searchUserProfiles(int, int[], int) - " + e);
            
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
            
            return null;
        }
        
        // Jobs that match the user's career desires exist, get the job details
        if (count > 0) {
            // Prepare the query to get job details for the matching jobs
            String query = "SELECT * FROM profile WHERE (";
            for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
                query += "SKILL" + Integer.toString(i + 1) + " BETWEEN ? AND ?)";
                if (i < UserProfile.SKILLS_SIZE - 1) {
                    query += " AND (";
                }
            }
         
            try {
                ps = connection.prepareStatement(query);

                for (int i = 1; i < UserProfile.SKILLS_SIZE + 1; i++) {
                    ps.setInt((2*i) - 1, skills[i - 1] - tolerance);
                    ps.setInt((2*i), skills[i - 1] + tolerance);
                }

                rs = ps.executeQuery();

                while ((rs.next()) && (userProfilesCounter < numberOfUserProfiles)) {
                    UserProfile userProfile = new UserProfile();

                    userProfile.setProfileID(rs.getInt("ID"));
                    userProfile.setUserID(rs.getInt("USERID"));
                    
                    int[] s = new int[UserProfile.SKILLS_SIZE];
                    for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
                        String param = "SKILL" + Integer.toString(i + 1);
                        s[i] = rs.getInt(param);
                    }
                    userProfile.setSkills(s);
                    
                    int[] c = new int[UserProfile.CAREER_SKILLS_SIZE];
                    for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                        String param = "CSKILL" + Integer.toString(i + 1);
                        c[i] = rs.getInt(param);
                    }
                    userProfile.setCareerSkills(c);
                    
                    userProfiles[userProfilesCounter] = userProfile;
                    userProfilesCounter++;
                }
                
                // Fill up the remaining jobs with null jobs
                if (userProfilesCounter < numberOfUserProfiles) {
                    while (userProfilesCounter < numberOfUserProfiles) {
                        userProfiles[userProfilesCounter] = new UserProfile();
                        userProfilesCounter++;
                    }
                }
            } catch (SQLException e) {
                System.err.println("UserProfileDB.searchUserProfiles(int, int[], int) - " + e);

                return null;
            } finally {
                DBUtil.closeResultSet(rs);
                DBUtil.closePreparedStatement(ps);
                pool.freeConnection(connection);            
            }
        } else {
            return null;
        }
        
        return userProfiles;
    }
    
    public static synchronized UserProfile searchUserProfileRandom(int numberOfUserProfiles, int[] skills, int tolerance) {
        // numberOfUserProfiles is the number of profiles to include in the set prior to picking a random one
        // skills[] are the skills from the job
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        int count = 0;
        int userProfilesCounter = 0;
                
        // Prepare the query to find out if any jobs exist in the Job DB that match the user's desired skills
        String queryCount = "SELECT COUNT(*) AS rows FROM profile WHERE (";
        for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
            queryCount += "SKILL" + Integer.toString(i + 1) + " BETWEEN ? AND ?)";
            if (i < UserProfile.SKILLS_SIZE - 1) {
                queryCount += " AND (";
            }
        }
       
        try {
            ps = connection.prepareStatement(queryCount);
            
            for (int i = 1; i < UserProfile.SKILLS_SIZE + 1; i++) {
                ps.setInt((2*i) - 1, skills[i - 1] - tolerance);
                ps.setInt((2*i), skills[i - 1] + tolerance);
            }
            
            rs = ps.executeQuery();
            
            if (rs.first()) {
                // At least one job was found matching the user's desired career skills
                count = rs.getInt("rows");            
            } else {              
                return null;
            }
        } catch (SQLException e) {
            System.err.println("UserProfile.searchUserProfiles(int, int[], int) - " + e);

            return null;
        } finally {
            DBUtil.closeResultSet(rs);
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
        
        // Jobs that match the user's career desires exist, get the job details
        if (count > 0) {
            // Prepare the query to get job details for the matching jobs
            String query = "SELECT * FROM profile WHERE (";
            for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
                query += "SKILL" + Integer.toString(i + 1) + " BETWEEN ? AND ?)";
                if (i < UserProfile.SKILLS_SIZE - 1) {
                    query += " AND (";
                }
            }
            
            ConnectionPool pool2 = ConnectionPool.getInstance();
            Connection connection2 = pool2.getConnection();
            PreparedStatement ps2 = null;
            ResultSet rs2 = null;

            try {
                ps2 = connection2.prepareStatement(query);

                for (int i = 1; i < UserProfile.SKILLS_SIZE + 1; i++) {
                    ps2.setInt((2*i) - 1, skills[i - 1] - tolerance);
                    ps2.setInt((2*i), skills[i - 1] + tolerance);
                }

                rs2 = ps2.executeQuery();
                
                UserProfile[] userProfiles = new UserProfile[count];

                while ((rs2.next()) && (userProfilesCounter < numberOfUserProfiles) && (userProfilesCounter < count)) {                  
                    UserProfile userProfile = new UserProfile();

                    userProfile.setProfileID(rs2.getInt("ID"));
                    userProfile.setUserID(rs2.getInt("USERID"));
                    
                    int[] s = new int[UserProfile.SKILLS_SIZE];
                    for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) {
                        String param = "SKILL" + Integer.toString(i + 1);
                        s[i] = rs2.getInt(param);
                    }
                    userProfile.setSkills(s);
                    
                    int[] c = new int[UserProfile.CAREER_SKILLS_SIZE];
                    for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                        String param = "CSKILL" + Integer.toString(i + 1);
                        c[i] = rs2.getInt(param);
                    }
                    userProfile.setCareerSkills(c);
                    
                    userProfiles[userProfilesCounter] = userProfile;
                    userProfilesCounter++;
                }
                
                // Randomly pick one of the user profiles
                Random random = new Random();
                int r = random.nextInt(userProfilesCounter) + 1;
                
                return userProfiles[r - 1];
            } catch (SQLException e) {
                System.err.println("UserProfileDB.searchUserProfiles(int, int[], int) - " + e);

                return null;
            } finally {
                DBUtil.closeResultSet(rs2);
                DBUtil.closePreparedStatement(ps2);
                pool.freeConnection(connection2);            
            }
        } else {
            return null;
        }
    }
}
