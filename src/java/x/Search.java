/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrDocument; 

/**
 *
 * @author astla
 */
public class Search extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String url = "/results.jsp";
        String message = "";
        
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");
        UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
        Employer employer = (Employer) session.getAttribute("employer");
        Job[] jobs = (Job[]) session.getAttribute("jobs");
        User[] users = (User[]) session.getAttribute("users");
        
        String searchQuery = "";
        
        if (employer == null) {
            // Normal user: search for jobs
            
            String urlString = "http://localhost:8983/solr/secureers";
            SolrClient solr = new HttpSolrClient.Builder(urlString).build();
            
            // Check if user provided one or more keywords to search
            String keywords = request.getParameter("keywords");
            if (!keywords.isEmpty()) {
                searchQuery += keywords;
            } else {
                searchQuery += "(*:*) ";
            }
            
            String noskills = request.getParameter("noskills");
            if (!(noskills.equalsIgnoreCase("on"))) {
                System.out.println("on");
                // Get desired career skill values and tolerance
                String tolerance = request.getParameter("tolerance");

                String[] careerSkills = new String[UserProfile.CAREER_SKILLS_SIZE];
                for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                    String param = "careerskill" + Integer.toString(i + 1);
                    careerSkills[i] = request.getParameter(param);
                }

                // Modify search query and incorporate tolerance
                for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                    searchQuery += "AND (skill" + Integer.toString(i + 1) + ":[" + 
                            (Integer.parseInt(careerSkills[i]) - Integer.parseInt(tolerance)) + " TO " + 
                            (Integer.parseInt(careerSkills[i]) + Integer.parseInt(tolerance)) + "]) ";
                }
            }
            
            SolrQuery query = new SolrQuery();
            query.setQuery(searchQuery);
            
            QueryResponse qr = null;
            try {
                qr = solr.query(query);
                SolrDocumentList results = qr.getResults();
                
                if (results.size() > 0) {
                    Job[] jobResults = new Job[results.size()];
                    int counter = 0;
                    
                    for (SolrDocument sd : results) {
                        // Get the job ID and job details
                        int jid = Integer.parseInt(sd.getFieldValue("id").toString());
                        Job j = new Job();

                        jobResults[counter] = JobDB.getJob(jid);
                        counter++;
                    }
                    
                    session.setAttribute("jobResults", jobResults);
                } else {
                    Job[] jobResults = null;
                    session.setAttribute("jobResults", jobResults);
                }
            } catch (SolrServerException ex) {
                message = "An error occurred while searching!";
                url = "/error.jsp";
                
                session.setAttribute("message", message);
            }

            session.setAttribute("user", user);
            session.setAttribute("userProfile", userProfile);
            session.setAttribute("employer", employer);
            session.setAttribute("jobs", jobs);
            session.setAttribute("users", users);
        } else {
            // Employer user: search for candidates
            String urlString = "http://localhost:8983/solr/candidates";
            SolrClient solr = new HttpSolrClient.Builder(urlString).build();
            
            // Get desired career skill values and tolerance
            String tolerance = request.getParameter("tolerance");
            
            String[] careerSkills = new String[UserProfile.CAREER_SKILLS_SIZE];
            for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                String param = "careerskill" + Integer.toString(i + 1);
                careerSkills[i] = request.getParameter(param);
            }
            
            // Modify search query and incorporate tolerance
            for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) {
                searchQuery += "(skill" + Integer.toString(i + 1) + ":[" + 
                        (Integer.parseInt(careerSkills[i]) - Integer.parseInt(tolerance)) + " TO " + 
                        (Integer.parseInt(careerSkills[i]) + Integer.parseInt(tolerance)) + "])";
                if (i < UserProfile.CAREER_SKILLS_SIZE - 1) {
                    searchQuery += " AND ";
                }
            }
            
            SolrQuery query = new SolrQuery();
            query.setQuery(searchQuery);
            
            QueryResponse qr = null;
            try {
                qr = solr.query(query);
                SolrDocumentList results = qr.getResults();
                
                if (results.size() > 0) {
                    User[] userResults = new User[results.size()];
                    int counter = 0;

                    for (SolrDocument sd : results) {
                        // Get the job ID and job details
                        int uid = Integer.parseInt(sd.getFieldValue("userid").toString());
                        User u = new User();

                        userResults[counter] = UserDB.getUser(uid);
                        counter++;
                    }
                    
                    session.setAttribute("userResults", userResults);
                } else {
                    User[] userResults = null;
                    session.setAttribute("userResults", userResults);
                }
            } catch (SolrServerException ex) {
                message = "An error occurred while searching!";
                url = "/error.jsp";
                
                session.setAttribute("message", message);
            }

            session.setAttribute("user", user);
            session.setAttribute("userProfile", userProfile);
            session.setAttribute("employer", employer);
            session.setAttribute("jobs", jobs);
            session.setAttribute("users", users);
        }        
                
        // Forward request/response to View
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
