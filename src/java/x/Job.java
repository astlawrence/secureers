/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package x;

/**
 *
 * @author astla
 */
public class Job {
    
    public final static int RESPONSIBILITIES_SIZE = 5;
    public final static int SKILLS_SIZE = 6;
    
    private int j_ID;
    private int j_UserID;
    private int j_EmployerID;
    private String j_Title;
    private String j_Summary;
    private String j_Salary;
    private String j_YearsExperience;
    private String[] j_Responsibilities;
    private int[] j_Skills;
    private String j_EmploymentType;
    private String j_City;
    private String j_State;
    private String j_Zip;
    private String j_Education;
    
    public Job() {
        this.j_ID = -1;
        this.j_UserID = -1;
        this.j_EmployerID = -1;
        this.j_Title = "";
        this.j_Summary = "";
        this.j_Salary = "";
        this.j_YearsExperience = "";
        this.j_Responsibilities = new String[RESPONSIBILITIES_SIZE];
        this.j_Skills = new int[SKILLS_SIZE];
        this.j_EmploymentType = "";
        this.j_City = "";
        this.j_State = "";
        this.j_Zip = "";
        this.j_Education = "";
    }
    
    public Job(int uid, int eid, String title, String summary, String salary, String yearsExperience, String[] responsibilities, int[] skills, String employmentType, String city, String state, String zip, String education) {
        this.j_ID = -1;
        this.j_UserID = uid;
        this.j_EmployerID = eid;
        this.j_Title = title;
        this.j_Summary = summary;
        this.j_Salary = salary;
        this.j_YearsExperience = yearsExperience;
        this.j_Responsibilities = responsibilities;
        this.j_Skills = skills;
        this.j_EmploymentType = employmentType;
        this.j_City = city;
        this.j_State = state;
        this.j_Zip = zip;
        this.j_Education = education;
    }
    
    public void setID(int id) {
        this.j_ID = id;
    }
    
    public int getID() {
        return this.j_ID;
    }
    
    public void setUserID(int uid) {
        this.j_UserID = uid;
    }
    
    public int getUserID() {
        return this.j_UserID;
    }
    
    public void setEmployerID(int eid) {
        this.j_EmployerID = eid;
    }
    
    public int getEmployerID() {
        return this.j_EmployerID;
    }
    
    public void setTitle(String title) {
        this.j_Title = title;
    }
    
    public String getTitle() {
        return this.j_Title;
    }
    
    public void setSummary(String summary) {
        this.j_Summary = summary;
    }
    
    public String getSummary() {
        return this.j_Summary;
    }
    
    public void setSalary(String salary) {
        this.j_Salary = salary;
    }
    
    public String getSalary() {
        return this.j_Salary;
    }
    
    public void setYearsExperience(String yearsExperience) {
        this.j_YearsExperience = yearsExperience;
    }
    
    public String getYearsExperience() {
        return this.j_YearsExperience;
    }
    
    public void setResponsibilities(String[] responsibilities) {
        this.j_Responsibilities = responsibilities;
    }
    
    public String[] getResponsibilities() {
        return this.j_Responsibilities;
    }
    
    public void setSkills(int[] skills) {
        this.j_Skills = skills;
    }
    
    public int[] getSkills() {
        return this.j_Skills;
    }
    
    public void setEmploymentType(String employmentType) {
        this.j_EmploymentType = employmentType;
    }
    
    public String getEmploymentType() {
        return this.j_EmploymentType;
    }
    
    public void setCity(String city) {
        this.j_City = city;
    }
    
    public String getCity() {
        return this.j_City;
    }
    
    public void setState(String state) {
        this.j_State = state;
    }
    
    public String getState() {
        return this.j_State;
    }
    
    public void setZip(String zip) {
        this.j_Zip = zip;
    }
    
    public String getZip() {
        return this.j_Zip;
    }
    
    public void setEducation(String education) {
        this.j_Education = education;
    }
    
    public String getEducation() {
        return this.j_Education;
    }
    
    @Override
    public String toString() {
        String r = "";
        for (int i = 0; i < RESPONSIBILITIES_SIZE; i++) {
            r += this.j_Responsibilities[i] + "\n";
        }
        
        String s = "";
        for (int i = 0; i < SKILLS_SIZE; i++) {
            s += Integer.toString(this.j_Skills[i]) + " ";
        }
        
        return this.j_ID + "\n" +
                this.j_UserID + "\n" +
                this.j_EmployerID + "\n" +
                this.j_Title + "\n" +
                this.j_Summary + "\n" +
                this.j_Salary + "\nYearsExp: " +
                this.j_YearsExperience + "\nResp: " +
                r  + "Skills: " +
                s + " \n" +
                this.j_EmploymentType + "\n" +
                this.j_City + "\n" +
                this.j_State + "\n" +
                this.j_Zip + "\n" +
                this.j_Education;
    }
}
