<%-- 
    Document   : index
    Created on : Nov 19, 2016, 6:28:46 PM
    Author     : astla
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="x.User" %>
<%@ page import="x.UserProfile "%>
<%@ page import="x.Employer" %>
<%@ page import="x.Job" %>
<%
    User user = (User) session.getAttribute("user");
    UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
    Employer employer = (Employer) session.getAttribute("employer");
    Job[] jobs = (Job[]) session.getAttribute("jobs");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>About</title>
        
        <link rel="shortcut icon" href="/x/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/x/favicon.ico" type="image/x-icon">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-custom.css">
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- jQuery -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
        <link rel="stylesheet" href="assets/css/jquery-ui-custom.css">      
    </head>

    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="assets/images/triad.png" alt="" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/x/">Home</a></li>
                        <% if (user == null) { %>
                        <li><a href="/x/about.jsp">About</a></li>
                        <% } %>
                        <% if (!(user == null)) { %>
                        <li><a href="/x/profile.jsp">Profile</a></li>
                        <li><a href="/x/search.jsp">Search</a></li>
                        <% } %>
                    </ul>
                    <% if (user == null) { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                    </ul>
                    <% } else { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${applicationContext.contextPath}/x/logout">Logout</a></li>
                    </ul>
                    <% } %>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <br />
                <div class="col-xs-2 col-xs-offset-1">
                    <img src="assets/images/security3.png" alt="About" class="img-responsive" />
                </div>
                <div class="col-xs-8">
                    <h1>About</h1>
                    <p>secureers strives to bring together information security professionals seeking new careers with employers who wish to quickly fill open positions with the best talent using a skills-based approach.</p>
                    <br />
                    <p>Both information security professionals and employers select a value for each information security skill.  Skills currently include penetration testing, reverse engineering, incident response, security engineering, cloud security, and application security.</p>
                    <br />
                    <p>Information security professionals perform a skills assessment and assign values to how well they feel they currently perform each skill.  They then assign values to the skills they desire in their next information security career.  For example, an information security professional serving in an incident response role would rate their current incident response skill high.  But let's say this same individual instead desires a career more focused on cloud and application security.  They would then rate each of these skills as high in their desired career skills.</p>
                    <br />
                    <p>Employers post positions using traditional position characteristics such as title, years of experience, position responsibilities, education, etc. but also assign skill values to the skills required to succeed in the position.</p>
                    <br />
                    <p>Both information securit professionals and employers are presented with positions or candidates that best match their needs.  This allows both to constantly and automatically find what's best for their respective needs and desires.</p>
                    <br />
                    <p>If waiting for an employer to post a dream position or if waiting for a dream candidate to magically appear isn't possible, then use the search feature to find a position matching desired career skills or a candidate who possesses the skills required for the position.</p>
                    <br />
                    <p><a href="/x/register.jsp" class="btn btn-primary btn-block">Register</a></p>
                </div>
            </div>
            <div class="row">
                <hr />
                <footer>
                    <p class="footer-margin-left">&copy; 2016 secureers</p>
                </footer>
            </div>
        </div>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>
