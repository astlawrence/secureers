<%-- 
    Document   : postposition
    Created on : Nov 29, 2016, 9:12:40 PM
    Author     : astla
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="x.User" %>
<%@ page import="x.UserProfile" %>
<%@ page import="x.Employer" %>
<%@ page import="x.Job" %>
<%
    //String message = (String) session.getAttribute("message"); 
    //if (message == null) message = "";
    User user = (User) session.getAttribute("user");
    UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
    Employer employer = (Employer) session.getAttribute("employer");
    Job[] jobs = (Job[]) session.getAttribute("jobs");
    Job jobEdit = (Job) session.getAttribute("job");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <% if (jobEdit == null) { %>
        <title>Post New Position</title>
        <% } else { %>
        <title>Edit Position</title>
        <% } %>

        <link rel="shortcut icon" href="/x/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/x/favicon.ico" type="image/x-icon">        
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-custom.css">
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- jQuery -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
        <link rel="stylesheet" href="assets/css/jquery-ui-custom.css">
    </head>

    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="assets/images/triad.png" alt="" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/x/">Home</a></li>
                        <% if (user == null) { %>
                        <li><a href="/x/about.jsp">About</a></li>
                        <% } %>
                        <% if (!(user == null)) { %>
                        <li><a href="/x/profile.jsp">Profile</a></li>
                        <li><a href="/x/search.jsp">Search</a></li>
                        <% } %>
                    </ul>
                    <% if (user == null) { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                    </ul>
                    <% } else { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/x/logout">Logout</a></li>
                    </ul>
                    <% } %>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <% if (jobEdit == null) { %>
                            Post New Position
                            <% } else { %>
                            Edit Position
                            <% } %>
                        </div>
                        <form action="/x/postposition" method="POST">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Title:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="title" id="title" type="text" placeholder="Title" maxlength="255" tabindex="1" />
                                        <% } else { %>
                                        <input class="form-control" name="title" id="title" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getTitle()) %>" maxlength="255" tabindex="1" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Summary:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <textarea class="form-control" rows="3" name="summary" id="summary" placeholder="Summary" tabindex="2"></textarea>
                                        <% } else { %>
                                        <textarea class="form-control" rows="3" name="summary" id="summary" tabindex="2"><%= Encode.forHtmlContent(jobEdit.getSummary()) %></textarea>
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Employment Type:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <select class="form-control" name="employmentType" id="employmentType" tabindex="">
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Contractor">Contractor</option>
                                            <option value="Intern">Intern</option>
                                        </select>
                                        <% } else { %>
                                        <% String empTypeVal = jobEdit.getEmploymentType().trim(); %>
                                        <select class="form-control" name="employmentType" id="employmentType" tabindex="">
                                            <% if (empTypeVal.equals("Full Time")) { %>
                                            <option value="Full Time" selected>Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Contractor">Contractor</option>
                                            <option value="Intern">Intern</option>
                                            <% } else if (empTypeVal.equals("Part Time")) { %>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time" selected>Part Time</option>
                                            <option value="Contractor">Contractor</option>
                                            <option value="Intern">Intern</option>
                                            <% } else if (empTypeVal.equals("Contractor")) { %>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Contractor" selected>Contractor</option>
                                            <option value="Intern">Intern</option>
                                            <% } else { %>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                            <option value="Contractor">Contractor</option>
                                            <option value="Intern" selected>Intern</option>
                                            <% } %>
                                        </select>
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Salary:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="salary" id="salary" type="text" placeholder="Salary" tabindex="3" />
                                        <% } else { %>
                                        <input class="form-control" name="salary" id="salary" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getSalary()) %>" tabindex="3" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Years of Experience:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="yearsExperience" id="yearsExperience" type="text" placeholder="Years of Experience" tabindex="4" />
                                        <% } else { %>
                                        <input class="form-control" name="yearsExperience" id="yearsExperience" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getYearsExperience()) %>" tabindex="4" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Education:
                                    </div>
                                    <div class="col-xs-9">
                                        <select class="form-control" name="education" id="education" tabindex="5">
                                            <% if (jobEdit == null) { %>
                                            <option value="None">None</option>
                                            <option value="High School">High School</option>
                                            <option value="Bachelor's Degree">Bachelor's Degree</option>
                                            <option value="Master's Degree">Master's Degree</option>
                                            <option value="Doctorate">Doctorate</option>
                                            <% } else { %>
                                            <% String educationVal = jobEdit.getEducation().trim(); %>
                                            <% if (educationVal.equals("None")) { %>
                                            <option value="None" selected>None</option>
                                            <option value="High School">High School</option>
                                            <option value="Bachelor's Degree">Bachelor's Degree</option>
                                            <option value="Master's Degree">Master's Degree</option>
                                            <option value="Doctorate">Doctorate</option>
                                            <% } else if (educationVal.equals("High School")) { %>
                                            <option value="None">None</option>
                                            <option value="High School" selected>High School</option>
                                            <option value="Bachelor's Degree">Bachelor's Degree</option>
                                            <option value="Master's Degree">Master's Degree</option>
                                            <option value="Doctorate">Doctorate</option>
                                            <% } else if (educationVal.equals("Bachelor's Degree")) { %>
                                            <option value="None">None</option>
                                            <option value="High School">High School</option>
                                            <option value="Bachelor's Degree" selected>Bachelor's Degree</option>
                                            <option value="Master's Degree">Master's Degree</option>
                                            <option value="Doctorate">Doctorate</option>
                                            <% } else if (educationVal.equals("Master's Degree")) { %>
                                            <option value="None">None</option>
                                            <option value="High School">High School</option>
                                            <option value="Bachelor's Degree">Bachelor's Degree</option>
                                            <option value="Master's Degree" selected>Master's Degree</option>
                                            <option value="Doctorate">Doctorate</option>
                                            <% } else { %>
                                            <option value="None">None</option>
                                            <option value="High School">High School</option>
                                            <option value="Bachelor's Degree">Bachelor's Degree</option>
                                            <option value="Master's Degree">Master's Degree</option>
                                            <option value="Doctorate" selected>Doctorate</option>
                                            <% } %>
                                            <% } %>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Location City:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="city" id="city" type="text" placeholder="City" tabindex="6" />
                                        <% } else { %>
                                        <input class="form-control" name="city" id="city" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getCity()) %>" tabindex="6" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Location State:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="state" id="state" type="text" placeholder="State" tabindex="7" />
                                        <% } else { %>
                                        <input class="form-control" name="state" id="state" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getState()) %>" tabindex="7" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Location Zip:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="zip" id="zip" type="text" placeholder="Zip Code" tabindex="8" />
                                        <% } else { %>
                                        <input class="form-control" name="zip" id="zip" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getZip()) %>" tabindex="8" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Responsibility:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="responsibility1" id="responsibility1" type="text" placeholder="(Responsibility)" tabindex="9" />
                                        <% } else { %>
                                        <input class="form-control" name="responsibility1" id="responsibility1" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getResponsibilities()[0]) %>" tabindex="9" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Responsibility:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="responsibility2" id="responsibility2" type="text" placeholder="(Responsibility)" tabindex="10" />
                                        <% } else { %>
                                        <input class="form-control" name="responsibility2" id="responsibility2" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getResponsibilities()[1]) %>" tabindex="10" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Responsibility:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="responsibility3" id="responsibility3" type="text" placeholder="(Responsibility)" tabindex="11" />
                                        <% } else { %>
                                        <input class="form-control" name="responsibility3" id="responsibility3" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getResponsibilities()[2]) %>" tabindex="11" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Responsibility:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="responsibility4" id="responsibility4" type="text" placeholder="(Responsibility)" tabindex="12" />
                                        <% } else { %>
                                        <input class="form-control" name="responsibility4" id="responsibility4" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getResponsibilities()[3]) %>" tabindex="12" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Responsibility:
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (jobEdit == null) { %>
                                        <input class="form-control" name="responsibility5" id="responsibility5" type="text" placeholder="(Responsibility)" tabindex="13" />
                                        <% } else { %>
                                        <input class="form-control" name="responsibility5" id="responsibility5" type="text" value="<%= Encode.forHtmlAttribute(jobEdit.getResponsibilities()[4]) %>" tabindex="13" />
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        Skills:
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label for="skill">Penetration Testing</label>
                                                        <p class="small">
                                                            How well can you perform reconnaissance, scanning, target enumeration, exploiting, looting, and persisting?  If you've mastered recon-ng, nmap, Metasploit, mimikatz, PowerShell Empire, and stealth you score well!
                                                        </p>
                                                        <input type="hidden" id="skill1" name="skill1">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div id="slider1" tabindex="14"></div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label for="skill">Reverse Engineering</label>
                                                        <p class="small">
                                                            How well can you reverse engineer software?  If you've mastered IDA Pro, OllyDbg, gdb, ldd, mona you score well! 
                                                        </p>
                                                        <input type="hidden" id="skill2" name="skill2">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div id="slider2" tabindex="15"></div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label for="skill">Incident Response</label>
                                                        <p class="small">
                                                            How well can you identify anomalous behavior, triage events, quarantine and remediate affected systems? Do you enjoy learning about new attack vectors, tactics, and techniques? 
                                                        </p>
                                                        <input type="hidden" id="skill3" name="skill3">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div id="slider3" tabindex="16"></div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label for="skill">Security Engineering</label>
                                                        <p class="small">
                                                            How well can you design and build security systems to specifications while adhering to regulations and aligning with architectural strategic vision?
                                                        </p>
                                                        <input type="hidden" id="skill4" name="skill4">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div id="slider4" tabindex="17"></div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label for="skill">Cloud Security</label>
                                                        <p class="small">
                                                            How well can you secure a cloud environment regardless of vendor and service model?  
                                                        </p>
                                                        <input type="hidden" id="skill5" name="skill5">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div id="slider5" tabindex="18"></div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <label for="skill">Application Security</label>
                                                        <p class="small">
                                                            How well can you secure the continuous integration and continuous delivery pipeline while performing code reviews, code analysis and remediation?
                                                        </p>
                                                        <input type="hidden" id="skill6" name="skill6">
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div id="slider6" tabindex="19"></div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-primary" tabindex="20">Submit</button>&nbsp;<a href="/x/profile.jsp" class="btn btn-primary" tabindex="21">Cancel</a>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <% if (!(jobEdit == null)) { %><a href="/x/editposition?job=<%= jobEdit.getID() %>&action=delete" class="btn btn-danger text-right">Delete</a><% } %>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!-- container -->
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $( function() {
                <% for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) { %>
                $( "#slider<%= i + 1 %>" ).slider({
                    range: "min",
                    <% if (!(jobEdit == null)) { %>
                    value: <%= jobEdit.getSkills()[i] %>,
                    <% } else { %>
                    value: 1,
                    <% } %>
                    min: 0,
                    max: 10,
                    step: 1,
                    slide: function( event, ui ) {
                        $( "#skill<%= i + 1 %>" ).val( ui.value );
                    }
                });
                $( "#skill<%= i + 1 %>" ).val( $( "#slider<%= i + 1 %>" ).slider( "value" ) );
                <% } %>
            } );
        </script>
    </body>
</html>
