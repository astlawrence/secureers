/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$( function() {
    $( "#slider1" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill1" ).val( ui.value );
        }
    });
    $( "#skill1" ).val( $( "#slider1" ).slider( "value" ) );
} );

$( function() {
    $( "#slider2" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill2" ).val( ui.value );
        }
    });
    $( "#skill2" ).val( $( "#slider2" ).slider( "value" ) );
} );

$( function() {
    $( "#slider3" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill3" ).val( ui.value );
        }
    });
    $( "#skill3" ).val( $( "#slider3" ).slider( "value" ) );
} );

$( function() {
    $( "#slider4" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill4" ).val( ui.value );
        }
    });
    $( "#skill4" ).val( $( "#slider4" ).slider( "value" ) );
} );

$( function() {
    $( "#slider5" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill5" ).val( ui.value );
        }
    });
    $( "#skill5" ).val( $( "#slider5" ).slider( "value" ) );
} );

$( function() {
    $( "#slider6" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill6" ).val( ui.value );
        }
    });
    $( "#skill6" ).val( $( "#slider6" ).slider( "value" ) );
} );

$( function() {
    $( "#slider7" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill7" ).val( ui.value );
        }
    });
    $( "#skill7" ).val( $( "#slider7" ).slider( "value" ) );
} );

$( function() {
    $( "#slider8" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill8" ).val( ui.value );
        }
    });
    $( "#skill8" ).val( $( "#slider8" ).slider( "value" ) );
} );

$( function() {
    $( "#slider9" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill9" ).val( ui.value );
        }
    });
    $( "#skill9" ).val( $( "#slider9" ).slider( "value" ) );
} );

$( function() {
    $( "#slider10" ).slider({
        range: "min",
        value: 1,
        min: 0,
        max: 10,
        step: 1,
        slide: function( event, ui ) {
            $( "#skill10" ).val( ui.value );
        }
    });
    $( "#skill10" ).val( $( "#slider10" ).slider( "value" ) );
} );