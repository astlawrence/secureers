<%-- 
    Document   : index
    Created on : Nov 19, 2016, 6:28:46 PM
    Author     : astla
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="x.User" %>
<%@ page import="x.UserProfile "%>
<%@ page import="x.Employer" %>
<%@ page import="x.Job" %>
<%
    User user = (User) session.getAttribute("user");
    UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
    Employer employer = (Employer) session.getAttribute("employer");
    Job[] jobs = (Job[]) session.getAttribute("jobs");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>secureers</title>
        
        <link rel="shortcut icon" href="/x/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/x/favicon.ico" type="image/x-icon">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-custom.css">
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- jQuery -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
        <link rel="stylesheet" href="assets/css/jquery-ui-custom.css">

        <!-- Jumbotron parallax effect sourced from http://www.bootply.com/103783 -->
        <script type='text/javascript'>
            $(document).ready(function() {
                var jumboHeight = $('.jumbotron').outerHeight();
                function parallax(){
                    var scrolled = $(window).scrollTop();
                    $('.bg').css('height', (jumboHeight-scrolled) + 'px');
                }
                $(window).scroll(function(e){
                    parallax();
                });
            });
        </script>       
    </head>

    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="assets/images/triad.png" alt="" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/x/">Home</a></li>
                        <% if (user == null) { %>
                        <li><a href="/x/about.jsp">About</a></li>
                        <% } %>
                        <% if (!(user == null)) { %>
                        <li><a href="/x/profile.jsp">Profile</a></li>
                        <li><a href="/x/search.jsp">Search</a></li>
                        <% } %>
                    </ul>
                    <% if (user == null) { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                    </ul>
                    <% } else { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${applicationContext.contextPath}/x/logout">Logout</a></li>
                    </ul>
                    <% } %>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="bg"></div>
        <div class="jumbotron">
            <div class="container text-center">
                <h1>secureers</h1>
                <p class="lead">Align your aspirations with employer<br />needs to find your next infosec career!</p>
            </div>
        </div>
        <!-- About -->
        <div class="container index-spacing">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-xs-2">
                            <img src="assets/images/award.png" alt="About" class="img-responsive" />
                        </div>
                        <div class="col-xs-10">
                            <h1>About</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 text-justify">
                            <p>secureers strives to bring together information security professionals seeking new careers with employers who wish to quickly fill open positions with the best talent using a skills-based approach.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Professionals -->
        <div class="container index-spacing">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 text-right">
                            <h1>Professionals</h1>
                        </div>
                        <div class="col-xs-2">
                            <img src="assets/images/equipment.png" alt="Professinoals" class="img-responsive" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 text-justify">
                            <p>Finding your next information security career starts registering an account and then creating a profile ranking your current skills to allow employers to find you, your desired career skills to match posted positions, or search posted positions based on any combination of skills desired!  Once you've found the position you're looking for, apply!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Employers -->
        <div class="container index-spacing">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-xs-2">
                            <img src="assets/images/geography.png" alt="Employers" class="img-responsive" />
                        </div>
                        <div class="col-xs-8">
                            <h1>Employers</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 text-justify">
                            <p>Finding information security talent is simple: register an account, create a position based on skills and post it!  Can't wait?  Search for talent that best matches the position and let them know you're interested!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Get Started -->
        <div class="container index-spacing">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 text-right">
                            <h1>Get Started</h1>
                        </div>
                        <div class="col-xs-2">
                            <a href="/x/register.jsp"><img src="assets/images/multimedia.png" alt="Get Started" class="img-responsive" /></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 col-xs-offset-2 text-justify">
                            <p>Getting started is simple: register an account and create a profile or position based on skills!</p>
                            <p class="text-right"><a href="/x/register.jsp" class="btn btn-primary">Register Now!</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <hr />
                <footer>
                    <p class="footer-margin-left">&copy; 2016 secureers</p>
                </footer>
            </div>
        </div>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>
