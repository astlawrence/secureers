
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="x.User" %>
<%@ page import="x.UserProfile" %>
<%@ page import="x.Employer" %>
<%
    String message = (String) session.getAttribute("message"); 
    if (message == null) message = "";
    
    User user = (User) session.getAttribute("user");
    Employer employer = (Employer) session.getAttribute("employer");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Search</title>
        
        <link rel="shortcut icon" href="/x/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/x/favicon.ico" type="image/x-icon">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-custom.css">
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- jQuery -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
        <link rel="stylesheet" href="assets/css/jquery-ui-custom.css">   
    </head>

    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="assets/images/triad.png" alt="" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/x/">Home</a></li>
                        <% if (user == null) { %>
                        <li><a href="/x/about.jsp">About</a></li>
                        <% } %>
                        <% if (!(user == null)) { %>
                        <li><a href="/x/profile.jsp">Profile</a></li>
                        <li><a href="/x/search.jsp">Search</a></li>
                        <% } %>
                    </ul>
                    <% if (user == null) { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                    </ul>
                    <% } else { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/x/logout">Logout</a></li>
                    </ul>
                    <% } %>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <% if (employer == null) { %>
                <h2>Search Positions</h2>
                <hr />
                <% } else { %>
                <h2>Search Candidates</h2>
                <hr />
                <% } %>
                <form action="/x/search" method="POST">
                    <% if (employer == null) { %>
                    <div class="row">
                        <div class="col-xs-3">
                            <p>Keywords:</p>
                        </div>
                        <div class="col-xs-9">
                            <input class="form-control" name="keywords" type="text" tabindex="1" />
                            <div class="checkbox right text-right"><input type="checkbox" name="noskills" id="noskills" tabindex="2" /><label class="small" for="noskills" style="padding-left: 0px;">Ignore Skills</label></div>
                        </div>
                    </div>
                    <hr />
                    <% } %>
                    <div class="row">
                        <div class="col-xs-3">
                            <p>Desired Skills Tolerance:</p>
                        </div>
                        <div class="col-xs-2">
                            <select class="form-control" name="tolerance" <% if (employer == null) { %>tabindex="3" <% } else { %>tabindex="1"<% } %>>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="list-group">
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="skill">Penetration Testing</label>
                                                <p class="small">
                                                    How would you rank your desire to perform network, web and mobile reconnaissance, scanning, target analysis, research, exploiting, looting, and persisting?
                                                </p>
                                                <input type="hidden" id="careerskill1" name="careerskill1">
                                            </div>
                                            <div class="col-md-6">
                                                <div id="careerslider1" <% if (employer == null) { %>tabindex="4"<% } else { %>tabindex="2"<% } %>></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="skill">Reverse Engineering</label>
                                                <p class="small">
                                                    How would you rank your desire to statically and dynamically analyze software and malware using debuggers, disassemblers, decompilers, Linux and Windows tools, and fuzzers?
                                                </p>
                                                <input type="hidden" id="careerskill2" name="careerskill2">
                                            </div>
                                            <div class="col-md-6">
                                                <div id="careerslider2" <% if (employer == null) { %>tabindex="5"<% } else { %>tabindex="3"<% } %>></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="skill">Incident Response</label>
                                                <p class="small">
                                                    How would you rank your desire to identify anomalous behavior, triage events, quarantine and remediate affected systems while learning new attack vectors, tactics, and techniques to better defend? 
                                                </p>
                                                <input type="hidden" id="careerskill3" name="careerskill3">
                                            </div>
                                            <div class="col-md-6">
                                                <div id="careerslider3" <% if (employer == null) { %>tabindex="6"<% } else { %>tabindex="4"<% } %>></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="skill">Security Engineering</label>
                                                <p class="small">
                                                    How would you rank your desire to plan, design and build security systems?
                                                </p>
                                                <input type="hidden" id="careerskill4" name="careerskill4">
                                            </div>
                                            <div class="col-md-6">
                                                <div id="careerslider4" <% if (employer == null) { %>tabindex="7"<% } else { %>tabindex="5<% } %>"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="skill">Cloud Security</label>
                                                <p class="small">
                                                    How would you rank your desire to secure all aspects of public, private, and hybrid clouds?
                                                </p>
                                                <input type="hidden" id="careerskill5" name="careerskill5">
                                            </div>
                                            <div class="col-md-6">
                                                <div id="careerslider5" <% if (employer == null) { %>tabindex="8"<% } else { %>tabindex="6"<% } %>></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="skill">Application Security</label>
                                                <p class="small">
                                                    How would you rank your desire to secure applications from the inside out by building security in, creating security stories and abuse cases, analyzing and remediating code for vulnerabilities, and securing DevOps?
                                                </p>
                                                <input type="hidden" id="careerskill6" name="careerskill6">
                                            </div>
                                            <div class="col-md-6">
                                                <div id="careerslider6" <% if (employer == null) { %>tabindex="9"<% } else { %>tabindex="7"<% } %>></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                        </div>
                    </div>
                    <button class="btn btn-block btn-primary" type="submit" <% if (employer == null) { %>tabindex="10"<% } else { %>tabindex="8"<% } %>>Search</button>
                </form>
                <br />
            </div>
        </div>
    </div> <!-- container -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $( function() {
            <% for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) { %>
            $( "#careerslider<%= i + 1 %>" ).slider({
                range: "min",
                value: 1,
                min: 0,
                max: 10,
                step: 1,
                slide: function( event, ui ) {
                    $( "#careerskill<%= i + 1 %>" ).val( ui.value );
                }
            });
            $( "#careerskill<%= i + 1 %>" ).val( $( "#careerslider<%= i + 1 %>" ).slider( "value" ) );
            <% } %>
        } );
    </script>
  </body>
</html>
