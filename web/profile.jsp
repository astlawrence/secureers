<%@ page import="java.util.ArrayList" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="x.User" %>
<%@ page import="x.UserProfile" %>
<%@ page import="x.Employer" %>
<%@ page import="x.Job" %>
<%
    User user = (User) session.getAttribute("user");
    UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
    Employer employer = (Employer) session.getAttribute("employer");
    Job[] jobs = (Job[]) session.getAttribute("jobs");
    User[] users = (User[]) session.getAttribute("users");
    String applyMessage = (String) session.getAttribute("applyMessage");
    if (applyMessage == null) applyMessage = "";
    session.removeAttribute("job");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Profile</title>

        <link rel="shortcut icon" href="/x/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/x/favicon.ico" type="image/x-icon">        
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-custom.css">
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- jQuery -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
        <link rel="stylesheet" href="assets/css/jquery-ui-custom.css">
    </head>

    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="assets/images/triad.png" alt="" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/x/">Home</a></li>
                        <% if (user == null) { %>
                        <li><a href="/x/about.jsp">About</a></li>
                        <% } %>
                        <% if (!(user == null)) { %>
                        <li><a href="/x/profile.jsp">Profile</a></li>
                        <li><a href="/x/search.jsp">Search</a></li>
                        <% } %>
                    </ul>
                    <% if (user == null) { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                    </ul>
                    <% } else { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="${applicationContext.contextPath}/x/logout">Logout</a></li>
                    </ul>
                    <% } %>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <br />
                    <div class="row">
                        <div class="col-xs-5">
                            <% if (!(user == null) && (!(employer == null))) { %>
                            <img src="assets/images/geography.png" alt="Employers" class="img-responsive" />
                            <% } else if (!(user == null) ) { %>
                            <img src="assets/images/equipment.png" alt="Professinoals" class="img-responsive" />
                            <% } %>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            &nbsp;<h1>Welcome,&nbsp;<br/><%= Encode.forHtmlContent(user.getFirstName()) %>!</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xs-9">
                    <br />
                    <% if (!(applyMessage == null) && (!(applyMessage.isEmpty()))) { %>
                    <p class="success" style="margin: 5px 0 25px 0; padding: 5px 0 5px 0;"><span class="glyphicon glyphicon-ok-circle"></span>&nbsp;&nbsp;<%= Encode.forHtmlContent(applyMessage) %></p>
                    <% session.removeAttribute("applyMessage"); %>
                    <% } %>
                    <!-- USER/EMPLOYER : INFORMATION -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#information" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-chevron-down"></span></a>&nbsp;&nbsp;My Information
                        </div>
                        <div id="information" class="collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Name:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <% if ((user == null) || (user.getFirstName() == null) || (user.getLastName() == null)) { %>
                                            <p>&nbsp;</p>
                                        <% } else { %>
                                        <p><%= Encode.forHtmlContent(user.getFirstName()) %>&nbsp;<%= Encode.forHtmlContent(user.getLastName()) %></p>
                                        <% } %>
                                    </div>
                                </div>
                                <% if (!(user == null) && (user.getEmployer() == true)) { %>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Employer:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <% if ((user == null) || (user.getEmployerName() == null)) { %>
                                        <p>&nbsp;</p>
                                        <% } else { %>
                                        <p><%= Encode.forHtmlContent(user.getEmployerName()) %></p>
                                        <% } %>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Employer ID:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <% if ((user == null) || (user.getEmployerID() == -1)) { %>
                                        <p>&nbsp;</p>
                                        <% } else { %>
                                        <p><%= user.getEmployerID() %></p>
                                        <% } %>
                                    </div>
                                </div>
                                <% } %>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Email:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (user == null) { %>
                                            <p>&nbsp;</p>
                                        <% } else { %>
                                            <p><%= Encode.forHtmlContent(user.getEmail()) %></p>
                                        <% } %>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Phone:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <% if ((user == null) || (user.getPhone() == null)) { %>
                                            <p>&nbsp;</p>
                                        <% } else { %>
                                            <p><%= Encode.forHtmlContent(user.getPhone()) %></p>
                                        <% } %>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Address:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <% if ((user == null) || (user.getAddress() == null) || (user.getCity() == null) || (user.getState() == null) || (user.getZip() == null)) { %>
                                            <p>&nbsp;</p>
                                        <% } else { %>
                                            <p>
                                                <%= Encode.forHtmlContent(user.getAddress()) %>
                                                <br />
                                                <%= Encode.forHtmlContent(user.getCity()) %><% if (!(user.getCity().equals(""))) { %>,<% } %>&nbsp;<%= Encode.forHtmlContent(user.getState()) %>&nbsp;&nbsp;<%= Encode.forHtmlContent(user.getZip()) %>
                                            </p>
                                        <% } %>
                                    </div>
                                </div>
                            </div> <!-- panel-body -->
                            <div class="panel-footer">
                                <a href="/x/info.jsp" class="btn btn-sm btn-primary">Edit My Information</a>&nbsp;<a class="btn btn-sm btn-primary" href="/x/changepassword.jsp">Change Password</a>
                                <!--<button type="submit" class="btn btn-sm btn-primary">Edit My Information</button>-->
                            </div>
                        </div>
                    </div><!-- panel panel-default -->
                    <% if (!(user == null) && (user.getEmployer() == false)) { %>
                    <!-- USER : SKILLS -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#skills" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-chevron-down"></span></a>&nbsp;&nbsp;My Skills
                        </div>
                        <div id="skills" class="collapse">
                            <div class="panel-body">
                                <form action="/x/skills" method="POST">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="skill">Penetration Testing</label>
                                                    <p class="small">
                                                        How well can you perform reconnaissance, scanning, target enumeration, exploiting, looting, and persisting?  If you've mastered recon-ng, nmap, Metasploit, mimikatz, PowerShell Empire, and stealth you score well!
                                                    </p>
                                                    <input type="hidden" id="skill1" name="skill1">
                                                </div>
                                                <div class="col-xs-6">
                                                    <div id="slider1"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="skill">Reverse Engineering</label>
                                                    <p class="small">
                                                        How well can you reverse engineer software?  If you've mastered IDA Pro, OllyDbg, gdb, ldd, mona you score well! 
                                                    </p>
                                                    <input type="hidden" id="skill2" name="skill2">
                                                </div>
                                                <div class="col-xs-6">
                                                    <div id="slider2"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="skill">Incident Response</label>
                                                    <p class="small">
                                                        How well can you identify anomalous behavior, triage events, quarantine and remediate affected systems? Do you enjoy learning about new attack vectors, tactics, and techniques? 
                                                    </p>
                                                    <input type="hidden" id="skill3" name="skill3">
                                                </div>
                                                <div class="col-xs-6">
                                                    <div id="slider3"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="skill">Security Engineering</label>
                                                    <p class="small">
                                                        How well can you design and build security systems to specifications while adhering to regulations and aligning with architectural strategic vision?
                                                    </p>
                                                    <input type="hidden" id="skill4" name="skill4">
                                                </div>
                                                <div class="col-xs-6">
                                                    <div id="slider4"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="skill">Cloud Security</label>
                                                    <p class="small">
                                                        How well can you secure a cloud environment regardless of vendor and service model?  
                                                    </p>
                                                    <input type="hidden" id="skill5" name="skill5">
                                                </div>
                                                <div class="col-xs-6">
                                                    <div id="slider5"></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <label for="skill">Application Security</label>
                                                    <p class="small">
                                                        How well can you secure the continuous integration and continuous delivery pipeline while performing code reviews, code analysis and remediation?
                                                    </p>
                                                    <input type="hidden" id="skill6" name="skill6">
                                                </div>
                                                <div class="col-xs-6">
                                                    <div id="slider6"></div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- panel-body -->
                                <div class="panel-footer">
                                    <!--<a class="btn btn-sm btn-primary" href="/x/skills">Save My Skills</a>-->
                                    <button type="submit" class="btn btn-sm btn-primary">Save My Skills</button>
                                </div>
                            </form>
                        </div>
                    </div><!-- panel panel-default -->
                    <!-- USER : JOBS MATCH -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#jobsmatch" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-chevron-down"></span></a>&nbsp;&nbsp;Available Positions Matching My Desired Skills
                        </div>
                        <% if (!(jobs == null)) { %>
                        <div id="jobsmatch">
                            <div class="panel-body">
                                <form action="/x/applyposition" method="GET">
                                <input type="hidden" name="userid" value="<%= user.getID() %>" />
                                <% for (int i = 0; i < jobs.length; i++) { %>
                                    <% if (!(jobs[i].getID() == -1)) { %>
                                        <% if (i >= 1) { %>
                                        <hr />
                                        <% } %>
                                        <% int jobID = jobs[i].getID(); 
                                            String path = "applyposition?job=" + jobID; %>
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <h4><b><%= jobs[i].getTitle() %></b></h4>
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <p class="small"><%= Encode.forHtmlContent(jobs[i].getCity()) %>&#44;&nbsp;<%= Encode.forHtmlContent(jobs[i].getState()) %>&nbsp;&nbsp;<%= Encode.forHtmlContent(jobs[i].getZip()) %></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <p>Employment Type:</p>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <p><%= Encode.forHtmlContent(jobs[i].getEmploymentType()) %></p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <p>Salary:</p>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <p>$<%= Encode.forHtmlContent(jobs[i].getSalary()) %></p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <p>Experience:</p>
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <p><%= Encode.forHtmlContent(jobs[i].getYearsExperience()) %> Years</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <p>Summary:</p>
                                                            </div>    
                                                            <div class="col-xs-9">
                                                                <p><%= Encode.forHtmlContent(jobs[i].getSummary()) %></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-2 text-right">
                                                <a href="<%= response.encodeURL(path) %>" class="btn btn-sm btn-primary">Apply</a>
                                            </div>
                                        </div>
                                    <% } %>
                                <% } %>
                                </form>
                            </div>
                        </div>
                        <% } %>
                    </div>
                    
                    <% } %>
                    <% if (!(user == null) && (user.getEmployer())) { %>   
                    <!-- EMPLOYER : POSTED POSITIONS -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#postedpositions" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-chevron-down"></span></a>&nbsp;&nbsp;Posted Positions 
                        </div>
                        <% if (jobs != null) { %>
                        <div id="postedpositions">
                            <div class="panel-body">
                                <form action="/x/editpositions" method="GET">
                                <input type="hidden" name="userid" value="<%= user.getID() %>" />
                                    <% for (int i = 0; i < jobs.length; i++) { %>
                                        <% if (!(jobs[i].getID() == -1)) { %>
                                            <% if (i >= 1) { %>
                                            <hr />
                                            <% } %>
                                            <% int jobID = jobs[i].getID(); 
                                                String path = "editposition?job=" + jobID; %>
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <h4><b><%= jobs[i].getTitle() %></b></h4>
                                                    <div class="row">
                                                        <div class="col-xs-10">
                                                            <p class="small"><%= Encode.forHtmlContent(jobs[i].getCity()) %>&#44;&nbsp;<%= Encode.forHtmlContent(jobs[i].getState()) %>&nbsp;&nbsp;<%= Encode.forHtmlContent(jobs[i].getZip()) %></p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <p>Employment Type:</p>
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <p><%= Encode.forHtmlContent(jobs[i].getEmploymentType()) %></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <p>Salary:</p>
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <p>$<%= Encode.forHtmlContent(jobs[i].getSalary()) %></p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <p>Required Experience:</p>
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <p><%= Encode.forHtmlContent(jobs[i].getYearsExperience()) %> Years</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <p>Summary:</p>
                                                                </div>    
                                                                <div class="col-xs-9">
                                                                    <p><%= Encode.forHtmlContent(jobs[i].getSummary()) %></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2 text-right top-right">
                                                    <a href="<%= response.encodeURL(path) %>" class="btn btn-sm btn-primary">Edit</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <% if (!(users == null)) { %>
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <p>Potential Candidate:</p>
                                                                    <% if ((!(users[i].getID() == -1)) && (!(users == null))) { 
                                                                    String contact = "contact?uid=" + Integer.toString(users[i].getID()) + "&job=" + jobID; %>
                                                                    <a href="<%= response.encodeURL(contact) %>" class="btn btn-sm btn-primary">Contact</a>
                                                                    <% } %>
                                                                </div>
                                                                <div class="col-xs-9">
                                                                    <% if (!(users[i].getID() == -1)) { %>
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <p><%= Encode.forHtmlContent(users[i].getFirstName()) %>&nbsp;<%= Encode.forHtmlContent(users[i].getLastName()) %></p>
                                                                        </div>
                                                                    </div>
                                                                    <% if (!(users[i].getCity() == null) && !(users[i].getState() == null) && !(users[i].getZip() == null) && !(users[i].getCity().isEmpty()) && !(users[i].getState().isEmpty()) && !(users[i].getZip().isEmpty())) { %>
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <p><%= Encode.forHtmlContent(users[i].getCity()) %>, <%= Encode.forHtmlContent(users[i].getState()) %>&nbsp;&nbsp;<%= Encode.forHtmlContent(users[i].getZip()) %></p>
                                                                        </div>
                                                                    </div>
                                                                    <% } %>
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <p><%= Encode.forHtmlContent(users[i].getEmail()) %></p>
                                                                        </div>
                                                                    </div>
                                                                    <% if (!(users[i].getPhone() == null)) { %>
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <p><%= Encode.forHtmlContent(users[i].getPhone()) %></p>
                                                                        </div>
                                                                    </div>
                                                                    <% } %>
                                                                    <% } else { %>
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <p>No candidates match this position at this time.</p>
                                                                        </div>
                                                                    </div>
                                                                    <% } %>
                                                                </div>
                                                            </div>
                                                            <% } else { %>
                                                            <div class="row">
                                                                <div class="col-xs-3">
                                                                    <p class="small">Potential Candidate:</p>
                                                                </div>
                                                                <div class="col-xs-9">
                                                                    <p class="small">No candidates match this position at this time.</p>
                                                                </div>
                                                            </div>
                                                            <% } %>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <% } %>
                                    <% } %>
                                </form>    
                            </div>
                        </div>
                        <% } %>
                    </div>
                    <!-- EMPLOYER : POST NEW POSITION -->
                    <div class="row">
                        <div class="col-xs-12">
                            <a class="btn btn-block btn-primary" href="postposition.jsp">Post New Position</a>
                        </div>
                        <hr />
                    </div>
                    <% } %>
                    <% if (!(user == null) && (user.getEmployer() == false)) { %>
                    <!-- USER : DESIRED CAREER ATTRIBUTES -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="#" class="dropdown-toggle" data-toggle="collapse" data-target="#careerskills" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-chevron-down"></span></a>&nbsp;&nbsp;My Desired Career Skills
                        </div>
                        <div id="careerskills" class="collapsed">
                            <div class="panel-body">
                                <form action="/x/careerskills" method="POST">
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="skill">Penetration Testing</label>
                                                <p class="small">
                                                    How would you rank your desire to perform network, web and mobile reconnaissance, scanning, target analysis, research, exploiting, looting, and persisting?
                                                </p>
                                                <input type="hidden" id="careerskill1" name="careerskill1">
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="careerslider1"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="skill">Reverse Engineering</label>
                                                <p class="small">
                                                    How would you rank your desire to statically and dynamically analyze software and malware using debuggers, disassemblers, decompilers, Linux and Windows tools, and fuzzers?
                                                </p>
                                                <input type="hidden" id="careerskill2" name="careerskill2">
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="careerslider2"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="skill">Incident Response</label>
                                                <p class="small">
                                                    How would you rank your desire to identify anomalous behavior, triage events, quarantine and remediate affected systems while learning new attack vectors, tactics, and techniques to better defend? 
                                                </p>
                                                <input type="hidden" id="careerskill3" name="careerskill3">
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="careerslider3"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="skill">Security Engineering</label>
                                                <p class="small">
                                                    How would you rank your desire to plan, design and build security systems?
                                                </p>
                                                <input type="hidden" id="careerskill4" name="careerskill4">
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="careerslider4"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="skill">Cloud Security</label>
                                                <p class="small">
                                                    How would you rank your desire to secure all aspects of public, private, and hybrid clouds?
                                                </p>
                                                <input type="hidden" id="careerskill5" name="careerskill5">
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="careerslider5"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <label for="skill">Application Security</label>
                                                <p class="small">
                                                    How would you rank your desire to secure applications from the inside out by building security in, creating security stories and abuse cases, analyzing and remediating code for vulnerabilities, and securing DevOps?
                                                </p>
                                                <input type="hidden" id="careerskill6" name="careerskill6">
                                            </div>
                                            <div class="col-xs-6">
                                                <div id="careerslider6"></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-sm btn-primary">Save My Desired Career Skills</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <% } %>       
                </div>
            </div>
            <hr>
            <footer>
                <p>&copy; 2016 secureers</p>
            </footer>
        </div> <!-- /container -->

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            $( function() {
                <% for (int i = 0; i < UserProfile.SKILLS_SIZE; i++) { %>
                $( "#slider<%= i + 1 %>" ).slider({
                    range: "min",
                    <% if (!(userProfile == null)) { %>
                    value: <%= userProfile.getSkill(i) %>,
                    <% } else { %>
                    value: 1,
                    <% } %>
                    min: 0,
                    max: 10,
                    step: 1,
                    slide: function( event, ui ) {
                        $( "#skill<%= i + 1 %>" ).val( ui.value );
                    }
                });
                $( "#skill<%= i + 1 %>" ).val( $( "#slider<%= i + 1 %>" ).slider( "value" ) );
                <% } %>
            } );
        </script>
        <script type="text/javascript">
            $( function() {
                <% for (int i = 0; i < UserProfile.CAREER_SKILLS_SIZE; i++) { %>
                $( "#careerslider<%= i + 1 %>" ).slider({
                    range: "min",
                    <% if (!(userProfile == null)) { %>
                    value: <%= userProfile.getCareerSkill(i) %>,
                    <% } else { %>
                    value: 1,
                    <% } %>
                    min: 0,
                    max: 10,
                    step: 1,
                    slide: function( event, ui ) {
                        $( "#careerskill<%= i + 1 %>" ).val( ui.value );
                    }
                });
                $( "#careerskill<%= i + 1 %>" ).val( $( "#careerslider<%= i + 1 %>" ).slider( "value" ) );
                <% } %>
            } );
        </script>
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>
