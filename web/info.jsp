<%-- 
    Document   : index
    Created on : Nov 19, 2016, 6:28:46 PM
    Author     : astla
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="x.User" %>
<%@ page import="x.UserProfile" %>
<%@ page import="x.Employer" %>
<%@ page import="x.Job" %>
<%
    //String message = (String) session.getAttribute("message"); 
    //if (message == null) message = "";
    User user = (User) session.getAttribute("user");
    UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
    Employer employer = (Employer) session.getAttribute("employer");
    Job[] jobs = (Job[]) session.getAttribute("jobs");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Edit My Information</title>
        
        <link rel="shortcut icon" href="/x/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/x/favicon.ico" type="image/x-icon">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-custom.css">
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- jQuery -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
        <link rel="stylesheet" href="assets/css/jquery-ui-custom.css">       
    </head>

    <body>
                <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="assets/images/triad.png" alt="" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/x/">Home</a></li>
                        <% if (user == null) { %>
                        <li><a href="/x/about.jsp">About</a></li>
                        <% } %>
                        <% if (!(user == null)) { %>
                        <li><a href="/x/profile.jsp">Profile</a></li>
                        <li><a href="/x/search.jsp">Search</a></li>
                        <% } %>
                    </ul>
                    <% if (user == null) { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                    </ul>
                    <% } else { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/x/logout">Logout</a></li>
                    </ul>
                    <% } %>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">Edit My Information</div>
                        <div class="panel-body">
                            <form action="/x/info" method="POST">
                            <% if (!(user == null) && (user.getEmployer() == true)) { %>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <p>&nbsp;</p>
                                        </div>
                                        <div class="col-xs-9">
                                            <p>Employer Account</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            Employer:
                                        </div>
                                        <div class="col-xs-9">
                                            <% if ((user == null) || (user.getEmployerName() == null)) { %>
                                            <input class="form-control" name="employerName" id="employerName" type="text" placeholder="Employer" tabindex="0" />
                                            <% } else { %>
                                            <input class="form-control" name="employerName" id="employerName" type="text" value="<%= Encode.forHtmlAttribute(user.getEmployerName()) %>" tabindex="0" />
                                            <% } %>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            Employer ID:
                                        </div>
                                        <div class="col-xs-9">
                                            <% if ((user == null) || (user.getEmployerID() == -1)) { %>
                                            <input class="form-control" name="employerID" id="employerID" type="text" placeholder="Employer" tabindex="0" />
                                            <% } else { %>
                                            <input class="form-control" name="employerID" id="employerID" type="text" value="<%= user.getEmployerID() %>" tabindex="0" />
                                            <% } %>
                                        </div>
                                    </div>
                                </div>
                                <% } %>
                            <div class="form-group">    
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Name:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <% if ((user == null) || (user.getFirstName() == null)) { %>
                                                    <input class="form-control" name="firstName" id="firstName" type="text" placeholder="First" tabindex="1" />
                                                <% } else { %>
                                                    <input class="form-control" name="firstName" id="firstName" type="text" value="<%= Encode.forHtmlAttribute(user.getFirstName()) %>" tabindex="1" />
                                                <% } %>
                                            </div>
                                            <div class="col-xs-6">
                                                <% if ((user == null) || (user.getLastName() == null)) { %>
                                                    <input class="form-control" name="lastName" id="lastName" type="text" placeholder="Last" tabindex="2" />
                                                <% } else { %>
                                                    <input class="form-control" name="lastName" id="lastName" type="text" value="<%= Encode.forHtmlAttribute(user.getLastName()) %>" tabindex="2" />
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <p>Email:</p>
                                </div>
                                <div class="col-xs-9">
                                    <% if (user == null) { %>
                                        <p>&nbsp;</p>
                                    <% } else { %>
                                        <p><%= Encode.forHtmlContent(user.getEmail()) %></p>
                                    <% } %>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Phone:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <% if ((user == null) || (user.getPhone() == null)) { %>
                                                    <input class="form-control" name="phone" id="phone" type="text" placeholder="Phone" tabindex="3" />
                                                <% } else { %>
                                                    <input class="form-control" name="phone" id="phone" type="text" value="<%= Encode.forHtmlAttribute(user.getPhone()) %>" tabindex="3" />
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Address:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <% if ((user == null) || (user.getAddress() == null)) { %>
                                                    <input class="form-control" name="address" id="address" type="text" placeholder="Address" tabindex="4" />
                                                <% } else { %>
                                                    <input class="form-control" name="address" id="address" type="text" value="<%= Encode.forHtmlAttribute(user.getAddress()) %>" tabindex="4" />
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>City:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <% if ((user == null) || (user.getCity() == null)) { %>
                                                    <input class="form-control" name="city" id="city" type="text" placeholder="City" tabindex="5" />
                                                <% } else { %>
                                                    <input class="form-control" name="city" id="city" type="text" value="<%= Encode.forHtmlAttribute(user.getCity()) %>" tabindex="5" />
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>State:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <% if ((user == null) || (user.getState() == null)) { %>
                                                    <input class="form-control" name="state" id="state" type="text" placeholder="State" maxlength="2" tabindex="6" />
                                                <% } else { %>
                                                    <input class="form-control" name="state" id="state" type="text" value="<%= Encode.forHtmlAttribute(user.getState()) %>" maxlength="2" tabindex="6" />
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Zip Code:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <% if ((user == null) || (user.getZip() == null)) { %>
                                                    <input class="form-control" name="zip" id="zip" type="text" placeholder="Zip Code" tabindex="7" />
                                                <% } else { %>
                                                    <input class="form-control" name="zip" id="zip" type="text" value="<%= Encode.forHtmlAttribute(user.getZip()) %>" tabindex="7" />
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- panel-body -->
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-sm btn-primary">Save My Information</button>&nbsp;<a href="/x/profile.jsp" class="btn btn-primary btn-sm">Cancel</a>
                        </div>
                        </form>
                    </div><!-- panel panel-default -->
                </div>
            </div>
        </div> <!-- container -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>
