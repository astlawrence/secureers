<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="x.User" %>
<%@ page import="x.UserProfile" %>
<%@ page import="x.Employer" %>
<%@ page import="x.Job" %>
<%
    String message = (String) session.getAttribute("message"); 
    if (message == null) message = "";
    
    User user = (User) session.getAttribute("user");
    Employer employer = (Employer) session.getAttribute("employer");
    Job[] jobResults = (Job[]) session.getAttribute("jobResults");
    User[] userResults = (User[]) session.getAttribute("userResults");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Search</title>
        
        <link rel="shortcut icon" href="/x/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/x/favicon.ico" type="image/x-icon">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-custom.css">
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- jQuery -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
        <link rel="stylesheet" href="assets/css/jquery-ui-custom.css">   
    </head>

    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="assets/images/triad.png" alt="" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/x/">Home</a></li>
                        <% if (user == null) { %>
                        <li><a href="/x/about.jsp">About</a></li>
                        <% } %>
                        <% if (!(user == null)) { %>
                        <li><a href="/x/profile.jsp">Profile</a></li>
                        <li><a href="/x/search.jsp">Search</a></li>
                        <% } %>
                    </ul>
                    <% if (user == null) { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                    </ul>
                    <% } else { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/x/logout">Logout</a></li>
                    </ul>
                    <% } %>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <h2>Search Results</h2>
                <hr />
                <% if ((!(jobResults == null)) && (employer == null)) {
                    for (int i = 0; i < jobResults.length; i++) { %>
                    <% if (i >= 1) { %>
                    <hr />
                    <% } %>
                    <% int jobID = jobResults[i].getID(); 
                        String path = "applyposition?job=" + jobID; %>
                    <div class="row">
                        <div class="col-xs-10">
                            <h4><b><%= Encode.forHtmlContent(jobResults[i].getTitle()) %></b></h4>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="small"><%= Encode.forHtmlContent(jobResults[i].getCity()) %>&#44;&nbsp;<%= Encode.forHtmlContent(jobResults[i].getState()) %>&nbsp;&nbsp;<%= Encode.forHtmlContent(jobResults[i].getZip()) %></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <p>Employment Type:</p>
                                            </div>
                                            <div class="col-xs-9">
                                                <p><%= Encode.forHtmlContent(jobResults[i].getEmploymentType()) %></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <p>Salary:</p>
                                            </div>
                                            <div class="col-xs-9">
                                                <p>$<%= Encode.forHtmlContent(jobResults[i].getSalary()) %></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <p>Experience:</p>
                                            </div>
                                            <div class="col-xs-9">
                                                <p><%= Encode.forHtmlContent(jobResults[i].getYearsExperience()) %> Years</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <p>Summary:</p>
                                            </div>    
                                            <div class="col-xs-9">
                                                <p><%= Encode.forHtmlContent(jobResults[i].getSummary()) %></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2 text-right">
                                <a href="<%= response.encodeURL(path) %>" class="btn btn-sm btn-primary">Apply</a>
                            </div>
                        </div>
                    <% } %>
                    <br />
                    <br />
                <% } else if ((!(userResults == null)) && (!(employer == null))) {
                        for (int i = 0; i < userResults.length; i++) { %>
                        <% if (i >= 1) { %>
                        <hr />
                        <% } %>
                        <% int userID = userResults[i].getID(); 
                            String path = "contact?uid=" + userID; %>
                        <div class="row">
                            <div class="col-xs-10">
                                <h4><b><%= Encode.forHtmlContent(userResults[i].getFirstName()) %>&nbsp;<%= Encode.forHtmlContent(userResults[i].getLastName()) %></b></h4>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Location:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <% if ((!(userResults[i].getCity() == null)) && (!(userResults[i].getState() == null)) && (!(userResults[i].getZip() == null)) && (!(userResults[i].getCity().isEmpty())) && (!(userResults[i].getState().isEmpty())) && (!(userResults[i].getZip().isEmpty()))) { %>
                                        <p><%= Encode.forHtmlContent(userResults[i].getCity()) %>&#44;&nbsp;<%= Encode.forHtmlContent(userResults[i].getState()) %>&nbsp;&nbsp;<%= Encode.forHtmlContent(userResults[i].getZip()) %></p>
                                        <% } %>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Email:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <p><%= Encode.forHtmlContent(userResults[i].getEmail()) %></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <p>Phone:</p>
                                    </div>
                                    <div class="col-xs-9">
                                        <% if (!(userResults[i].getPhone() == null)) { %>
                                        <p><%= Encode.forHtmlContent(userResults[i].getPhone()) %></p>
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2 text-right">
                                <a href="<%= response.encodeURL(path) %>" class="btn btn-sm btn-primary">Contact</a>
                            </div>
                        </div>
                    <% } %>
                    <br />
                    <br />
                <% } else { %>
                <p><h4><b>No results!</b></h4></p>
                <% } %>
            </div>
        </div>
    </div> <!-- container -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>
