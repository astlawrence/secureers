
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="x.User" %>
<%@ page import="x.UserProfile" %>
<%@ page import="x.Employer" %>
<%@ page import="x.Job" %>
<%
    String message = (String) session.getAttribute("message"); 
    if (message == null) message = "A general error has occurred!";
    
    User user = (User) session.getAttribute("user");
    UserProfile userProfile = (UserProfile) session.getAttribute("userProfile");
    Employer employer = (Employer) session.getAttribute("employer");
    Job[] jobs = (Job[]) session.getAttribute("jobs");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Post New Position</title>

        <link rel="shortcut icon" href="/x/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/x/favicon.ico" type="image/x-icon">        
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-custom.css">
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- jQuery -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
        <link rel="stylesheet" href="assets/css/jquery-ui-custom.css">
        
        <!-- Sliders -->
        <script src="assets/js/sliders.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="assets/images/triad.png" alt="" /></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/x/">Home</a></li>
                        <% if (user == null) { %>
                        <li><a href="/x/about.jsp">About</a></li>
                        <% } %>
                        <% if (!(user == null)) { %>
                        <li><a href="/x/profile.jsp">Profile</a></li>
                        <li><a href="/x/search.jsp">Search</a></li>
                        <% } %>
                    </ul>
                    <% if (user == null) { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                    </ul>
                    <% } else { %>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/x/logout">Logout</a></li>
                    </ul>
                    <% } %>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <br />
                    <p class="alert text-center" style="color: red; margin: 5px 0 25px 0; padding: 5px 0 5px 0;"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;&nbsp;<%= message %></p>
                </div>
            </div>
        </div><!-- container -->
    
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>
